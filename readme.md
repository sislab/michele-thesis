# Michele Thesis

Repository containing Michele Yin thesis and future works

## Folder structure
```bash
Elicitation
├─ LLMs-runner
│  ├─ data_analysis
│  │  ├─ json_format # data analysis for narrative elicitation
│  │  │  ├─ img 
│  │  │  ├─ img_vertical   
│  │  │  ├─ tables
│  │  └─ txt_format # data analysis for story cloze test
│  │     ├─ img
│  │     ├─ img_vertical   
│  │     ├─ tables   
│  ├─ dataset
│  │  ├─ roc_cleanup.txt
│  │  ├─ narrative_elicitation # datasets of narrative elicitations
│  ├─ environment.yml # env file in conda
│  ├─ prompts
│  │  ├─ narrative_elicitation
│  │  │  ├─ prompts.toml # copy of prompts that were used
│  │  │  └─ token_length_check.ipynb # check that the lenght is below 2048
│  │  ├─ readme.md
│  │  └─ story_cloze
│  │     └─ prompts.toml # copy of prompts that were used
│  ├─ results
│  │  ├─ old_dataset # old invalid results for story cloze 
│  │  │  ├─ first_experiment # 0 shot
│  │  │  ├─ second_experiment # 3 shot
│  │  │  └─ third_experiment # 3 shot with 1 sentence length
│  │  ├─ cleaned_dataset # results of Story cloze
│  │  │  ├─ full # prompt is present in the input data
│  │  │  │  ├─ first_experiment # 0 shot
│  │  │  │  ├─ second_experiment # 3 shot
│  │  │  │  └─ third_experiment # 3 shot with 1 sentence length
│  │  │  └─ stripped # only the answers is present
│  │  │  │  ├─ first_experiment # 0 shot
│  │  │  │  ├─ second_experiment # 3 shot
│  │  │  │  └─ third_experiment # 3 shot with 1 sentence length
│  │  ├─ narrative_elicitation # data for narrative elicitation results
│  │  ├─ post_processing # post processing scripts
│  └─ runner # run the file
│     ├─ json_format # narrative elicitation
│     │  ├─ chatGPT
│     │  │  ├─ ChatGPT.py # chapt gpt runner throguh API
│     │  │  └─ config # config files
│     │  ├─ huggingface
│     │  │  ├─ runner.py # hugginface pipeline generation runner script
│     │  │  └─ config # config files
│     └─ txt_format # sotry cloze
│        ├─ chatGPT
│        │  ├─ ChatGPT.py # chapt gpt runner throguh API
│        │  └─ config # config files
│        ├─ huggingface
│        │  ├─ runner.py # hugginface pipeline generation runner script
│        │  └─ config # config files
│        └─ oracle # generate oracle files with llms results structure
│           ├─ oracle.py 
│           └─ oracle_config.toml
├─ dataset # coadapt dataset
├─ human_evaluation
│  ├─ batch_generation/ # script used to generate the batches 
│  ├─ hacking_server/ # steal the data because issues with the platform
│  ├─ data_analysis/ # script used to analyse the data
│  └─ data # human evaluated data
├─ finetuning # Fine tuning and Future works for the coadapt dataset
├─ lm_master_disi_en # thesis tex file
│  ├─ assets
│  │  ├─ imgs/*
│  │  └─ table/*
│  └─ lm_master_disi_en/*
└─ readme.md

```