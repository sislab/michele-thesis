import json
import re
import numpy as np
from datasets import Dataset, DatasetDict
from transformers import (
    AutoTokenizer,
)

"""
Script to process the dataset from a json file for the narrative generation task and prepare it for the language modeling task and similar things.
"""

# dataset generator for Transformers
def gen(path: str):
    """
    loads the json for the dataset file
    """
    with open(path, "rb") as f:
        data = json.load(f)
    for i in data:
        yield i


def preprocess_function(
    examples: dict[list], prompt: str = "{narrative}"
) -> dict[list]:
    """
    function to preprocess the dataset, merging the input with the generated questions and formatting the text according to the prompt.
    :examples: dict of lists with this format
    :prompt: prompt to format the text. Must contain {narrative}. The question will be added at the end

    output: dict of lists with this format

    input format entries
    ```json
    [
        {
            "input": "Ciao, tutto bene molto lavoro il questi ultimi giorni. ",
            "output": [
                {
                    "generated_text": "Sono contento di sentire che tutto \u00e8 ok, su cosa stai lavorando?"
                },
                {
                    "generated_text": "Mi fa piacere vada tutto bene. Di cosa ti occupi?"
                }
            ],
            "id": "1",
            "highlight_positive": [
                "tutto bene"
            ],
            }
        ...
    ]
    ```

    and outputs with this format augmenting the input with the generated text and increasing the number of samples
    output format entries
    ```json
    [
        {
            "input": "Ciao, tutto bene molto lavoro il questi ultimi giorni. ",
            "id": "1",
            "highlight_positive": [
                "tutto bene"
            ],
            "question" : "Sono contento di sentire che tutto \u00e8 ok, su cosa stai lavorando?",
            "full": "Ciao, tutto bene molto lavoro il questi ultimi giorni. Sono contento di sentire che tutto \u00e8 ok, su cosa stai lavorando?"
            "prompt_text" : "[] Ciao, tutto bene molto lavoro il questi ultimi giorni. []" # FORMATTED according to prompt
        },
        {
            "input": "Ciao, tutto bene molto lavoro il questi ultimi giorni. ",
            "id": "1",
            "highlight_positive": [
                "tutto bene"
            ],
            'question': 'Mi fa piacere vada tutto bene. Di cosa ti occupi?',
            "full": "Ciao, tutto bene molto lavoro il questi ultimi giorni. Mi fa piacere vada tutto bene. Di cosa ti occupi?",
            "prompt_text" : "[] Ciao, tutto bene molto lavoro il questi ultimi giorni. []"
        },
        ...
    ]
    ```
    """
    if "{narrative}" not in prompt:
        raise ValueError("prompt must contain {narrative} to format correctly")
    n_samples = len(examples["input"])
    examples["full"] = []
    examples["question"] = []
    examples["prompt_text"] = []
    # process the full text with the first question
    for idx in range(n_samples):
        #  prompt the input
        prompt_text = prompt.format(narrative=examples["input"][idx])
        # separate the full
        full = prompt_text + examples["output"][idx][0]["generated_text"]
        # add to the examples
        examples["prompt_text"].append(prompt_text)
        examples["full"].append(full)
        examples["question"].append(examples["output"][idx][0]["generated_text"])
    # add the rest of the questions to the examples
    for idx in range(n_samples):
        for out in examples["output"][idx][1:]:
            # tokenize separately the input and the question
            prompt_text = prompt.format(narrative=examples["input"][idx])
            # separate the full
            full = prompt_text + out["generated_text"]
            # add the original input to the examples
            examples["input"].append(examples["input"][idx])
            examples["id"].append(examples["id"][idx])
            examples["highlight_positive"].append(examples["highlight_positive"][idx])
            examples["highlight_negative"].append(examples["highlight_negative"][idx])
            # add the output
            examples["prompt_text"].append(prompt_text)
            examples["full"].append(full)
            examples["question"].append(out["generated_text"])

    return examples


def alternate_preprocess_function(
    examples: dict[list], prompt: str = "{narrative}"
) -> dict[list]:
    """
    function to preprocess the dataset keeping the questions in a list for each sample. Useful for evaluation purposes for BLEU and similar metrics.
    :prompt: prompt to format the text. Must contain {narrative}. The question will be added at the end

    output: dict of lists with this format

    input format entries
    ```json
    [
        {
            "input": "Ciao, tutto bene molto lavoro il questi ultimi giorni. ",
            "output": [
                {
                    "generated_text": "Sono contento di sentire che tutto \u00e8 ok, su cosa stai lavorando?"
                },
                {
                    "generated_text": "Mi fa piacere vada tutto bene. Di cosa ti occupi?"
                }
            ],
            "id": "1",
            "highlight_positive": [
                "tutto bene"
            ],
            }
        ...
    ]
    ```

    and outputs with this format augmenting the input with the generated text and increasing the number of samples
    output format entries
    ```json
    [
        {
            "input": "Ciao, tutto bene molto lavoro il questi ultimi giorni. ",
            "id": "1",
            "highlight_positive": [
                "tutto bene"
            ],
            "questions" : ["Sono contento di sentire che tutto \u00e8 ok, su cosa stai lavorando?",'Mi fa piacere vada tutto bene. Di cosa ti occupi?'],
            "fulls": ["Ciao, tutto bene molto lavoro il questi ultimi giorni. Sono contento di sentire che tutto \u00e8 ok, su cosa stai lavorando?","Ciao, tutto bene molto lavoro il questi ultimi giorni. Mi fa piacere vada tutto bene. Di cosa ti occupi?"]
            "prompt_text" : "[] Ciao, tutto bene molto lavoro il questi ultimi giorni. [],"
        },
        ...
    ]
    ```
    """
    if "{narrative}" not in prompt:
        raise ValueError("prompt must contain {narrative} to format correctly")
    n_samples = len(examples["input"])
    examples["full"] = []
    examples["question"] = []
    examples["prompt_text"] = []
    # process the full text with the first question
    for idx in range(n_samples):
        #  prompt the input
        prompt_text = prompt.format(narrative=examples["input"][idx])

        fulls = []
        questions = []

        for out in examples["output"][idx]:
            # separate the full
            full = prompt_text + out["generated_text"]
            fulls.append(full)
            questions.append(out["generated_text"])
            # add to the examples
        examples["prompt_text"].append(prompt_text)
        examples["full"].append(fulls)
        examples["question"].append(questions)

    return examples


def highlight_preprocess_function(
    examples: dict[list],
    prompt_positive: str = "[VERDE]({positive})",
    prompt_negative: str = "[ROSSO]({negative})",
) -> dict[list]:
    """
    function to change the dataset prompt and format the highlighted text in the narratives with a special prompt
    :examples: dict of lists with this format
    :prompt: prompt to format the text. Must contain {narrative}. The question will be added at the end

    output: dict of lists with this format

    input format entries
    ```json
    [
        {
            "input": "Ciao, tutto bene molto lavoro il questi ultimi giorni. ",
            "output": [
                {
                    "generated_text": "Sono contento di sentire che tutto \u00e8 ok, su cosa stai lavorando?"
                },
                {
                    "generated_text": "Mi fa piacere vada tutto bene. Di cosa ti occupi?"
                }
            ],
            "id": "1",
            "highlight_positive": [
                "tutto bene"
            ],
            }
        ...
    ]
    ```

    ```json
    [
        {
            "input": "Ciao, [VERDE](tutto bene) molto lavoro il questi ultimi giorni. ",
            "output": [
                {
                    "generated_text": "Sono contento di sentire che tutto \u00e8 ok, su cosa stai lavorando?"
                },
                {
                    "generated_text": "Mi fa piacere vada tutto bene. Di cosa ti occupi?"
                }
            ],
            "id": "1",
            "highlight_positive": [
                "tutto bene"
            ],
            }
        ...
    ]
    ```
    """
    assert (
        "{positive}" in prompt_positive
    ), "prompt_positive must contain {positive} to format correctly"
    assert (
        "{negative}" in prompt_negative
    ), "prompt_negative must contain {negative} to format correctly"
    n_samples = len(examples["input"])
    for idx in range(n_samples):
        for positive in examples["highlight_positive"][idx]:
            examples["input"][idx] = examples["input"][idx].replace(
                positive, prompt_positive.format(positive=positive)
            )
        for negative in examples["highlight_negative"][idx]:
            examples["input"][idx] = examples["input"][idx].replace(
                negative, prompt_negative.format(negative=negative)
            )

    return examples

def token_balance_preprocess_function(
    examples:dict[list]
) -> dict[list]:
    """
    function to balance the dataset token distribution to avoid bad training biases. Removes 90% of the samples that start with 'mi dispiace' randomly
    """
    copy = {}
    for idx in range(len(examples["input"])):
        # if the first two tokens according to this regex are 'mi dispiace'
        if ' '.join(re.split('[\s,.!?]+',examples['question'][idx].lower())[:2]) == 'mi dispiace':
            rng = np.random.default_rng()
            # 90% drop this sample
            drop = rng.choice([True,False],2, p=[0.9,0.1])
        else:
            # else keep it
            drop = [False,False]
        # copy the data if not dropping
        if not drop[0]:
            for k,v in examples.items():
                if k not in copy:
                    copy[k] = []
                copy[k].append(v[idx])
    return copy
def causal_processing(
    examples: dict[list], tokenizer: AutoTokenizer, block_size: int = 512
) -> dict[list]:
    """
    applies to tokenizer to the data for causal language modeling
    """
    # set the size of the block to the max length of the input_ids and removes the remainder
    tokenized = tokenizer(
        examples["full"], 
        # padding=True,
        padding='max_length', 
        truncation=True, 
        max_length=block_size
    )
    examples["input_ids"] = tokenized["input_ids"]
    examples["attention_mask"] = tokenized["attention_mask"]
    examples["labels"] = examples["input_ids"].copy()
    return examples


def seq2seq_processing(
    examples: dict[list], tokenizer: AutoTokenizer, block_size: int = 512
) -> dict[list]:
    """
    applies to tokenizer to the data for seq2seq language modeling
    """
    # set the size of the block to the max length of the input_ids and removes the remainder
    tokenized = tokenizer(
        examples["prompt_text"],
        # padding=True,
        padding='max_length',
        truncation=True,
        max_length=block_size,
        
    )
    examples["input_ids"] = tokenized["input_ids"]
    examples["attention_mask"] = tokenized["attention_mask"]
    tokenized_labels = tokenizer(
        examples["question"],
        # padding=True,
        padding='max_length',
        truncation=True,
        max_length=block_size,
    )
    examples["labels"] = tokenized_labels["input_ids"].copy()
    return examples


def narrative_dataset(
    tokenizer: AutoTokenizer,
    task_type: str,
    train_path: str = None,
    test_path: str = None,
    prompt: str = "{narrative}",
    batch_size : int = 8,
    block_size: int = 512,
    call_first: callable = None,
    call_back: callable = None,
    fn_kwargs_call_first: dict = None,
    fn_kwargs_call_back: dict = None,
):
    """
    Function to process the dataset in the format required by the model, either for CausalLM or Seq2Seq

    tokenizer: tokenizer to use
    task_type: type of task to perform, one of "causal" or "seq2seq"
    train_path: path to the training dataset
    test_path: path to the test dataset
    prompt: prompt to add to the full text
    block_size: max length of the input_ids. Will be padded to the right
    call_first: custom callfirst function for the map dataset to use instead of the default ones
    call_back: custom callback function for the map dataset to use instead of the default ones
    fn_kwargs_call_first: kwargs to pass to the callfirst map dataset function
    fn_kwargs_call_back: kwargs to pass to the callback map dataset function
    """
    if task_type == "causal":
        post_processing = causal_processing
    elif task_type == "seq2seq":
        post_processing = seq2seq_processing
    elif call_back is None:
        raise ValueError(
            "task_type must be one of 'causal' or 'seq2seq' or a custom callback function must be provided"
        )

    if train_path is None and test_path is None:
        raise ValueError("At least one of train_path or test_path must be provided")
    paths = []
    if train_path is not None:
        paths.append(train_path)
    if test_path is not None:
        paths.append(test_path)
    # prepare dataset
    # add pad as eos if not present
    if tokenizer.pad_token is None:
        tokenizer.pad_token = tokenizer.eos_token
    if tokenizer.bos_token is None:
        tokenizer.bos_token = tokenizer.eos_token
    datasets = []
    for path in paths:
        ds = Dataset.from_generator(
            generator=gen,
            gen_kwargs={"path": path},
        )
        
        # custom preprocess
        if call_first is not None:
            ds = ds.map(
                call_first,
                batched=True,
                batch_size=batch_size,
                fn_kwargs=fn_kwargs_call_first,
                load_from_cache_file=False)
            
        # remove output to prevent issues with data missmatch of empty lists and split the dataset to add the prompt
        splitted_dataset = ds.map(
            preprocess_function,
            batched=True,
            batch_size=batch_size,
            remove_columns=["output"],
            fn_kwargs={"prompt": prompt},
            load_from_cache_file=False
        )

        # tokenize and add lm labels according to the task
        tokenized_dataset = splitted_dataset.map(
            post_processing,
            batched=True,
            batch_size=batch_size,
            fn_kwargs={"tokenizer": tokenizer, "block_size": block_size},
            load_from_cache_file=False
        )
        # add custom post process callback
        if call_back is not None:
            tokenized_dataset = tokenized_dataset.map(
                call_back,
                batched=True,
                batch_size=batch_size,
                fn_kwargs=fn_kwargs_call_back,
                load_from_cache_file=False)
        datasets.append(tokenized_dataset)
    if test_path is None:
        lm_dataset = DatasetDict(
            {
                "train": datasets[0],
            }
        )
        return lm_dataset
    if train_path is None:
        lm_dataset = DatasetDict(
            {
                "test": datasets[0],
            }
        )
        return lm_dataset
    lm_dataset = DatasetDict(
        {
            "train": datasets[0],
            "test": datasets[1],
        }
    )
    
    return lm_dataset


def narrative_dataset_untokenized(
    train_path: str = None,
    test_path: str = None,
    prompt: str = "{narrative}",
    call_first: callable = None,
    call_back: callable = None,
    fn_kwargs_call_first: dict = None,
    fn_kwargs_call_back: dict = None,
):
    """
    Function to process the dataset splitting the multiple questions in different samples and formatting the text according to the prompt.

    train_path: path to the training dataset
    test_path: path to the test dataset
    prompt: prompt to add to the full text
    call_back: custom callback function for the map dataset to use instead of the default ones
    fn_kwargs: kwargs to pass to the callback map dataset function
    """
    if train_path is None and test_path is None:
        raise ValueError("At least one of train_path or test_path must be provided")
    paths = []
    if train_path is not None:
        paths.append(train_path)
    if test_path is not None:
        paths.append(test_path)
    # prepare dataset

    datasets = []
    for path in paths:
        ds = Dataset.from_generator(
            generator=gen,
            gen_kwargs={"path": path},
        )
        # custom preprocess
        if call_first is not None:
            ds = ds.map(
                call_first,
                batched=True,
                fn_kwargs=fn_kwargs_call_first,
                load_from_cache_file=False)
            
        # remove output to prevent issues with data missmatch of empty lists
        splitted_dataset = ds.map(
            preprocess_function,
            batched=True,
            remove_columns=["output"],
            fn_kwargs={"prompt": prompt},
            load_from_cache_file=False
        )
        if call_back is not None:
            splitted_dataset = splitted_dataset.map(
                call_back,
                batched=True,
                fn_kwargs=fn_kwargs_call_back,
                load_from_cache_file=False)
        datasets.append(splitted_dataset)
    if test_path is None:
        lm_dataset = DatasetDict(
            {
                "train": datasets[0],
            }
        )
        return lm_dataset
    if train_path is None:
        lm_dataset = DatasetDict(
            {
                "test": datasets[0],
            }
        )
        return lm_dataset
    lm_dataset = DatasetDict(
        {
            "train": datasets[0],
            "test": datasets[1],
        }
    )
    return lm_dataset


def narrative_dataset_raw(
    train_path: str = None,
    test_path: str = None,
    prompt: str = "{narrative}",
    call_first: callable = None,
    call_back: callable = None,
    fn_kwargs_call_first: dict = None,
    fn_kwargs_call_back: dict = None,
):
    """
    Function to process the dataset without splitting the multiple questions.

    train_path: path to the training dataset
    test_path: path to the test dataset
    call_back: custom callback function for the map dataset to use instead of the default ones
    fn_kwargs: kwargs to pass to the callback map dataset function
    """
    if train_path is None and test_path is None:
        raise ValueError("At least one of train_path or test_path must be provided")
    paths = []
    if train_path is not None:
        paths.append(train_path)
    if test_path is not None:
        paths.append(test_path)
    # prepare dataset

    datasets = []
    for path in paths:
        ds = Dataset.from_generator(
            generator=gen,
            gen_kwargs={"path": path},
        )
        # custom preprocess
        if call_first is not None:
            ds = ds.map(
                call_first,
                batched=True,
                fn_kwargs=fn_kwargs_call_first,
                load_from_cache_file=False)
    
        # remove output to prevent issues with data missmatch of empty lists
        splitted_dataset = ds.map(
            alternate_preprocess_function,
            batched=True,
            remove_columns=["output"],
            fn_kwargs={"prompt": prompt},
            load_from_cache_file=False
        )
        if call_back is not None:
            splitted_dataset = splitted_dataset.map(
                call_back,
                batched=True,
                fn_kwargs=fn_kwargs_call_back,
                load_from_cache_file=False)
        datasets.append(splitted_dataset)
    if test_path is None:
        lm_dataset = DatasetDict(
            {
                "train": datasets[0],
            }
        )
        return lm_dataset
    if train_path is None:
        lm_dataset = DatasetDict(
            {
                "test": datasets[0],
            }
        )
        return lm_dataset
    lm_dataset = DatasetDict(
        {
            "train": datasets[0],
            "test": datasets[1],
        }
    )
    return lm_dataset


if __name__ == "__main__":
    import os
    from argparse import ArgumentParser

    argparser = ArgumentParser()
    argparser.add_argument("--dataset", type=str, required=True)
    args = argparser.parse_args()
    assert os.path.exists(args.dataset), "Ground truth file not found."
    tokenizer = AutoTokenizer.from_pretrained("gsarti/it5-small")
    # just loads the dataset to a dataset object, keeps the questions in a list
    test_raw = narrative_dataset_raw(test_path=args.dataset)["test"]
    # separates the questions in different samples but it does not tokenize them
    test_untokenized = narrative_dataset_untokenized(test_path=args.dataset)["test"]
    # tokenizes the dataset for causal language modeling
    test_dataset = narrative_dataset(
        tokenizer=tokenizer, test_path=args.dataset, task_type="causal"
    )["test"]
    # tokenizes the dataset for causal language modeling and adds a special prompt for the highlighted text with a custom preprocess function that is called before the default one
    test_call_first = narrative_dataset(
        tokenizer=tokenizer,
        test_path=args.dataset,
        task_type="causal",
        call_first=highlight_preprocess_function,
        fn_kwargs_call_first={
            "prompt_positive": "[VERDE]({positive})",
            "prompt_negative": "[ROSSO]({negative})",
        },
    )["test"]
    # tokenizes the dataset for causal language modeling and with a custom postprocess function that is called after the end of the default one to remove some samples ( in this case the ones that start with 'mi dispiace' )
    test_call_back = narrative_dataset(
        tokenizer=tokenizer,
        test_path=args.dataset,
        task_type="causal",
        call_back=token_balance_preprocess_function,
    )["test"]
    # import pdb;pdb.set_trace()