import json
import os
import sys
from argparse import ArgumentParser

import evaluate

# add narrativeDataset.py file to the path
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))
from narrative_dataset import narrative_dataset_raw


if __name__ == "__main__":
    argparser = ArgumentParser()
    argparser.add_argument("--predictions_file", type=str, required=True)
    argparser.add_argument("--ground_truth_dataset", type=str, required=True)
    argparser.add_argument("--save_path", type=str, required=True)
    argparser.add_argument("--max_order", type=int, default=1)
    # argparser.add_argument("--prompt",type=str, default="{narrative}")
    args = argparser.parse_args()
    assert os.path.exists(args.predictions_file), "Predictions file not found."
    assert os.path.exists(args.ground_truth_dataset), "Ground truth file not found."
    os.makedirs(os.path.dirname(args.save_path), exist_ok=True)
    with open(args.predictions_file, "rb") as f:
        predictions = json.load(f)["predictions"]
    test = narrative_dataset_raw(test_path=args.ground_truth_dataset)["test"]
    ground = [l for l in test["question"]][: len(test)]
    predictions = predictions[: len(test)]
    # import pdb;pdb.set_trace()
    bleu = evaluate.load("bleu")
    results_processed = bleu.compute(
        predictions=predictions, references=ground, max_order=args.max_order
    )
    with open(args.save_path, "w") as f:
        json.dump(results_processed, f, indent=4)
