# Finetuning of LLMs using custom Narrative dataset

# Requirements
The requirements are the followings
```bash
-python==3.11 # to parse the toml config file
-torch # machine learning
-datasets # huggingface 
-evaluate
-transformers 
-wandb # optional wandb for logging
-optuna # optuna for hyperparameters search
```
You can use the 

```
conda env create -f environment.yml
```

to create and use the env
activate the env by 
```
conda activate finetuning
```
You might need to install your torch libraries according to the platform you are using
```
pip3 install torch torchvision torchaudio
# or
pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118
# or
pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu121
```

### Dataset

The dataset is composed of data entries in a JSON format
#### Raw
```json
[
    {
        "input": "Ciao tutto bene una giornata in pieno relax. ",
        "output": [
            {
                "generated_text": "Che cosa hai fatto in questa giornata di relax?"
            }
        ],
        "id": "0",
        "highlight_positive": [
            "una giornata in pieno relax"
        ],
        "highlight_negative": []
    },
    {
        "input": "Ciao, tutto bene molto lavoro il questi ultimi giorni. ",
        "output": [
            {
                "generated_text": "Sono contento di sentire che tutto \u00e8 ok, su cosa stai lavorando?"
            },
            {
                "generated_text": "Mi fa piacere vada tutto bene. Di cosa ti occupi?"
            }
        ],
        "id": "1",
        "highlight_positive": [
            "tutto bene"
        ],
        "highlight_negative": []
    },
    ...
    ]
```
#### Processing
The `narrative_dataset.py` file automatically prepares the dataset for a Language Modelling task.
The result is a list of entries, each narrative paired with a different question. Takes as input the tokenizer, the two paths of train and test set, if the task is for CausalLM or Seq2SeqLM and a prompt format. A few extra fields are also introduced:

- `prompt_text` is the input narrative formatted according to the prompt
- `full` is the `prompt_text` to which the question is appended
- `question`. The  `output` is replaced with `question`. Question no longer contains multiple questions for the same narrative. The total number of narratives are therefore augmented as the narratives with more than one question are separated as different entries.
- `input_ids`. This field behaves differently for CausalLM and Seq2SeqLM. 
  - for CausalLM the `input_ids` is the `full` field of each data entry tokenized. 
  - for Seq2SeqLM the `input_ids` is the `prompt_text` field of each data entry tokenized.
- `labels`. This field also behaves differently for CausalLM and Seq2SeqLM.
  - for CausalLM the `labels` is the `full` field of each data entry tokenized. Is the same as `input_ids`. The CausalLM collator will shift them right of 1 token.
  - for Seq2SeqLM the `labels` is the `question` field of each data entry tokenized.

These differences in behavior allow for an easier deployment of fine tuning. Also notice that as a starked difference between large corpora training, these examples are not collated. Each batch is padded with 0s but is not collated. **Each sample is kept separate**.

For more information check the comments in the `narrative_dataset.py` and the example usage and the end of the file.
```json
    [
        {
            "input": "Ciao, tutto bene molto lavoro il questi ultimi giorni. ",
            "id": "1",
            "highlight_positive": [
                "tutto bene"
            ],
            "question" : "Sono contento di sentire che tutto \u00e8 ok, su cosa stai lavorando?",
            "full": "Ciao, tutto bene molto lavoro il questi ultimi giorni. Sono contento di sentire che tutto \u00e8 ok, su cosa stai lavorando?",
            "prompt_text" : "Ciao, tutto bene molto lavoro il questi ultimi giorni.",
            "input_ids" : [1,2,3,43 ...], 
            "label_ids" : [1,2,3,43 ...]
        },
        {
            "input": "Ciao, tutto bene molto lavoro il questi ultimi giorni. ",
            "id": "1",
            "highlight_positive": [
                "tutto bene"
            ],
            "question": "Mi fa piacere vada tutto bene. Di cosa ti occupi?",
            "full": "Ciao, tutto bene molto lavoro il questi ultimi giorni. Mi fa piacere vada tutto bene. Di cosa ti occupi?",
            "prompt_text" : "Ciao, tutto bene molto lavoro il questi ultimi giorni. ",
            "input_ids" : [1,2,3,43 ...], 
            "label_ids" : [1,2,3,43 ...]
        },
        ...
    ]
```
### fine tuning

Fine tuning is done by calling the script with the following.
```bash
# example of CausalLM
 python 'finetuning/CausalLM/narrativeDatasetTrainCausalLM.py' --model LorenzoDeMattei/GePpeTto --train dataset/narrative_elicitation/train_set_h.json --test dataset/narrative_elicitation/test_set_h.json --save_path checkpoints/geppetto --prompt NARRATIVA:{narrative}DOMANDA:  --lr  1.6445120710169317e-06 --wd 0.059412154121591135 --noise_tune 0.0921801456698458
 # example of Seq2SeqLM
 python 'finetuning/Seq2SeqLM/narrativeDatasetTrainSeq2SeqLM.py' --model gsarti/it5-small --train dataset/narrative_elicitation/train_set_h.json --test dataset/narrative_elicitation/test_set_h.json --save_path checkpoints/it5-small --prompt {narrative} --lr 6.943659742283259e-05 --wd 0.03528834421874781 --noise_tune 0.092
```
Commands like `CUDA_VISIBLE_DEVICES` are supported, so

```bash
CUDA_VISIBLE_DEVICES=0 python 'finetuning/CausalLM/narrativeDatasetTrainCausalLM.py' --model LorenzoDeMattei/GePpeTto --train dataset/narrative_elicitation/train_set_h.json --test dataset/narrative_elicitation/test_set_h.json --save_path checkpoints/geppetto --prompt NARRATIVA:{narrative}DOMANDA:  --lr  1.6445120710169317e-06 --wd 0.059412154121591135 --noise_tune 0.0921801456698458
```
will train only on the first GPU
### inference
Two types of inference are supported. Live terminal UI
```bash
python  narrativeDatasetInference.py --model 'distilgpt2' --train 'dataset/narrative_elicitation/train_set_h.json' --test 'dataset/narrative_elicitation/test_set_h.json'  --checkpoint 'checkpoint_path'
```
Or offloaded inference.
```bash
# CausalLM
python finetuning/CausalLM/narrativeDatasetCausalLMInference.py --model LorenzoDeMattei/GePpeTto --checkpoint checkpoint_path --dataset dataset/narrative_elicitation/test_set_h.json --prompt "NARRATIVA:{narrative}DOMANDA:" --save_path save_prediction_path
# Seq2SeqLM
python finetuning/Seq2SeqLM/narrativeDatasetSeq2SeqLMInference.py --model gsarti/it5-small --checkpoint checkpoint_path --dataset dataset/narrative_elicitation/test_set_h.json --prompt "{narrative}" --save_path save_prediction_path
```
### Evaluation 

```bash
# CausalLM
python finetuning/CausalLM/narrativeDatasetPerplexityCausalLM.py --model LorenzoDeMattei/GePpeTto --checkpoint checkpoint_path --dataset dataset/narrative_elicitation/test_set_h.json --prompt NARRATIVA:{narrative}DOMANDA: --save_path save_perplexity_path
# Seq2SeqLM
python finetuning/Seq2SeqLM/narrativeDatasetPerplexitySeq2SeqLM.py --model gsarti/it5-small --checkpoint checkpoint_path --dataset dataset/narrative_elicitation/test_set_h.json --prompt {narrative} --save_path save_perplexity_path
````