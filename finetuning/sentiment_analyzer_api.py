from ekphrasis.classes.preprocessor import TextPreProcessor
from ekphrasis.classes.tokenizer import SocialTokenizer
from ekphrasis.dicts.emoticons import emoticons

import pandas as pd
import re

import torch
import torch.nn.functional as F, torch.nn as nn
from torch.utils.data import DataLoader

from datasets import Dataset
import datasets
import logging

datasets.logging.get_verbosity = lambda: logging.NOTSET

from transformers import AutoTokenizer, AutoModel


class AlBERTo_Preprocessing(object):
    def __init__(self, do_lower_case=True):
        self.do_lower_case = do_lower_case
        self.text_processor = TextPreProcessor(
            # terms that will be normalized
            normalize=['url', 'email', 'user', 'percent', 'money', 'phone', 'time', 'date', 'number'],
            # terms that will be annotated
            annotate={"hashtag"},
            fix_html=True,  # fix HTML tokens
            unpack_hashtags=True,  # perform word segmentation on hashtags
            # select a tokenizer. You can use SocialTokenizer, or pass your own
            # the tokenizer, should take as input a string and return a list of tokens
            tokenizer=SocialTokenizer(lowercase=True).tokenize,
            dicts=[emoticons]
        )

    def preprocess(self, text):
        if self.do_lower_case:
            text = text.lower()
        text = str(" ".join(self.text_processor.pre_process_doc(text)))
        text = re.sub(r'[^a-zA-ZÀ-ú</>!?♥♡\s\U00010000-\U0010ffff]', ' ', text)
        text = re.sub(r'\s+', ' ', text)
        text = re.sub(r'(\w)\1{2,}', r'\1\1', text)
        text = re.sub(r'^\s', '', text)
        text = re.sub(r'\s$', '', text)
        return text


class AMC(nn.Module):
    def __init__(self):
        super(AMC, self).__init__()
        self.model = AutoModel.from_pretrained("m-polignano-uniba/bert_uncased_L-12_H-768_A-12_italian_alb3rt0")

        self.dropout1 = nn.Dropout(0.1)
        self.linear1 = nn.Linear(768, 3)

    def forward(self, input_ids, attention_mask, token_type_ids, **args):
        outputs = self.model(input_ids=input_ids, attention_mask=attention_mask, token_type_ids=token_type_ids, **args)
        outputs = self.dropout1(outputs[1])
        logits = self.linear1(outputs)
        return logits


class SentimentAnalyzerHF():
    def __init__(self, model,
                 tokenizer, device="cuda:0"):
        """
        :param str weights_path: path to pretrained weights
        :param str tok_path:     path/url to pretrained tokenizer
        :param str max_lenght:   max_lenght of the tokenizer
        :param str device:       either cpu or a cuda device
        """
        self.device = device

        self.preprocessing = AlBERTo_Preprocessing(do_lower_case=True)

        self.model = model.to(device)

        self.model.eval()

        self.tok = tokenizer

        self.map = {0: 'negative', 1: 'neutral', 2: 'positive'}

    def tokenize_function(self, examples):
        return self.tok(examples["text"], padding="max_length", truncation=True)

    def predict(self, dataloader, has_labels, logging=False):
        """
        :param DataLoader dataloader: Torch DataLoader object to be fed to the model
        :param bool has_labels:       True if we provide labels to compute the loss on
        :param bool logging:          True if we want to print the final loss over all samples. has_labels must be also True
        :return:                      Tuple with (classes_predicted, probability_vectors)
        """

        cumulative_loss = 0.
        classes, proba = [], []
        with torch.no_grad():
            for data in dataloader:
                input_ids, attention_mask, token_type_ids, labels = [data[key].to(self.device) for key in
                                                                     ["input_ids", "attention_mask", "token_type_ids",
                                                                      "labels"]]
                if not has_labels:
                    labels = None
                logits = self.model(labels, input_ids, attention_mask)['logits']
                classes.extend(logits.argmax(-1).cpu().detach().numpy().tolist())
                proba.extend(F.softmax(logits, dim=1).cpu().detach().numpy().tolist())

        return classes, proba

    def __call__(self, sentences, labels=None, batch_size=32, loss=False):
        """
        :param list sentences:             list of sentences to classify
        :param list labels:                list of G.T. labels
        :param list batch_size:            DataLoader's batch_size
        :return:                           Tuple with (classes_predicted, probability_vectors)
        """
        assert type(sentences) is list

        if labels is None:
            labels = [-1] * len(sentences)
            has_labels = False
        else:
            has_labels = True

        # prepare data
        sentences = [self.preprocessing.preprocess(s) for s in sentences]
        X = pd.DataFrame({"text": sentences, "idx": list(range(len(sentences))), "labels": labels})
        X = Dataset.from_pandas(X)
        X = X.map(self.tokenize_function, batched=True)

        # run the model
        preds = self.predict(
            DataLoader(
                X.with_format("torch", columns=["input_ids", "attention_mask", "labels", "token_type_ids"]),
                batch_size=batch_size
            ),
            has_labels=has_labels,
        )
        if not loss:
            sentiment = preds[0]
            return [self.map[x] for x in sentiment]
        else:
            return preds


class SentimentAnalyzer():
    def __init__(self, weights_path='data/models/amc_opt_msmd.pt',
                 tok_path="m-polignano-uniba/bert_uncased_L-12_H-768_A-12_italian_alb3rt0",
                 max_length=128, device="cuda:0"):
        """
        :param str weights_path: path to pretrained weights
        :param str tok_path:     path/url to pretrained tokenizer
        :param str max_lenght:   max_lenght of the tokenizer
        :param str device:       either cpu or a cuda device
        """
        self.device = device

        self.preprocessing = AlBERTo_Preprocessing(do_lower_case=True)

        self.model = AMC().to(device)
        self.model.load_state_dict(torch.load(weights_path))
        self.model.eval()

        self.tok = AutoTokenizer.from_pretrained(tok_path)
        self.tok.model_max_length = max_length
        self.map = {0: 'negative', 1: 'neutral', 2: 'positive'}

    def tokenize_function(self, examples):
        return self.tok(examples["text"], padding="max_length", truncation=True)

    def predict(self, dataloader, has_labels, logging=False):
        """
        :param DataLoader dataloader: Torch DataLoader object to be fed to the model
        :param bool has_labels:       True if we provide labels to compute the loss on
        :param bool logging:          True if we want to print the final loss over all samples. has_labels must be also True
        :return:                      Tuple with (classes_predicted, probability_vectors)
        """

        cumulative_loss = 0.
        classes, proba = [], []
        with torch.no_grad():
            for data in dataloader:
                input_ids, attention_mask, token_type_ids, labels = [data[key].to(self.device) for key in
                                                                     ["input_ids", "attention_mask", "token_type_ids",
                                                                      "labels"]]
                logits = self.model(input_ids, attention_mask, token_type_ids)

                if has_labels and logging:
                    loss = F.cross_entropy(logits, labels)
                    cumulative_loss += loss.detach()

                classes.extend(logits.argmax(-1).cpu().detach().numpy().tolist())
                proba.extend(F.softmax(logits, dim=1).cpu().detach().numpy().tolist())

        return classes, proba

    def __call__(self, sentences, labels=None, batch_size=32, loss=False):
        """
        :param list sentences:             list of sentences to classify
        :param list labels:                list of G.T. labels
        :param list batch_size:            DataLoader's batch_size
        :return:                           Tuple with (classes_predicted, probability_vectors)
        """
        assert type(sentences) is list

        if labels is None:
            labels = [-1] * len(sentences)
            has_labels = False
        else:
            has_labels = True

        # prepare data
        sentences = [self.preprocessing.preprocess(s) for s in sentences]
        X = pd.DataFrame({"text": sentences, "idx": list(range(len(sentences))), "labels": labels})
        X = Dataset.from_pandas(X)
        X = X.map(self.tokenize_function, batched=True)

        # run the model
        preds = self.predict(
            DataLoader(
                X.with_format("torch", columns=["input_ids", "attention_mask", "labels", "token_type_ids"]),
                batch_size=batch_size
            ),
            has_labels=has_labels,
        )
        if not loss:
            sentiment = preds[0]
            return [self.map[x] for x in sentiment]
        else:
            return preds