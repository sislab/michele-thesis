"""
Inference script for Seq2Seq LM
"""
import json
import os
import sys
import re

import torch

if torch.cuda.is_available():
    device = "cuda"
elif torch.backends.mps.is_available():
    device = "mps"
else:
    device = "cpu"

import warnings
from argparse import ArgumentParser
from transformers import (
    AutoModelForSeq2SeqLM,
    AutoTokenizer,
    pipeline,
)

# add narrativeDataset.py file to the path
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))
from narrative_dataset import narrative_dataset, highlight_preprocess_function


if __name__ == "__main__":
    parser = ArgumentParser(
        prog="Inference Evaluator for Seq2SeqLMs", description="Run a model"
    )
    parser.add_argument(
        "--model",
        type=str,
        required=True,
        help="Model to use for fine tuning from huggingface.co/models",
    )
    parser.add_argument(
        "--checkpoint",
        type=str,
        help="Path to load the checkpoints if any, otherwise use the pre-trained model",
    )
    parser.add_argument(
        "--dataset", type=str, help="Path to the dataset", required=True
    )
    parser.add_argument(
        "--prompt", type=str, default="", help="Prompt to add to the full text"
    )
    parser.add_argument(
        "--prompt_positive",
        type=str,
        default="{positive}",
        help="Prompt to format the hightlighted positive. Default is {positive}, which is no formatting",
    )
    parser.add_argument(
        "--prompt_negative",
        type=str,
        default="{negative}",
        help="Prompt to format the hightlighted negative. Default is {negative}, which is no formatting",
    )
    parser.add_argument(
        "--save_path",
        type=str,
        default="predictions.json",
        help="Path to save the evaluation results",
    )

    parser.add_argument(
        "--device",
        type=str,
        default=device,
        help="Device to use. Default CUDA or MPS if available, otherwise CPU",
    )

    args = parser.parse_args()

    device = args.device
    # if checkpoint is provided check if it exists
    if args.checkpoint:
        assert os.path.exists(args.checkpoint), f"File {args.checkpoint} not found"
        assert os.path.isdir(args.checkpoint), f"Path {args.checkpoint} is not a file"
    else:
        warnings.warn("No checkpoint provided, using base pre-trained model instead")
    assert os.path.exists(args.dataset) and os.path.isfile(
        args.dataset
    ), f"File {args.dataset} not found"

    dir = os.path.dirname(args.save_path)
    os.makedirs(dir, exist_ok=True)
    if os.path.isdir(args.save_path):
        args.save_path = os.path.join(args.save_path, "bleu.json")
    tokenizer = AutoTokenizer.from_pretrained(args.model)
    tokenizer.padding_side = "right"

    model = AutoModelForSeq2SeqLM.from_pretrained(
        args.checkpoint if args.checkpoint else args.model
    )
    pipe = pipeline(
        task="text2text-generation",
        model=model,
        tokenizer=tokenizer,
        max_new_tokens=128,
        device=device,
    )

    test = narrative_dataset(
        tokenizer,
        task_type="seq2seq",
        test_path=args.dataset,
        prompt=args.prompt,
        call_first=highlight_preprocess_function,
        fn_kwargs_call_first={
            "prompt_positive": args.prompt_positive,
            "prompt_negative": args.prompt_negative,
        },
    )["test"]
    generation = pipe(test["prompt_text"])

    def stripper(processed: str) -> str:
        processed = re.split("(\?)", processed)
        try:
            processed = "".join(processed[0:2])
        except IndexError:
            processed = processed[0]
        return processed

    processed = [l["generated_text"] for l in generation]

    data_dict = {
        "model_name": args.model,
        "checkpoint": args.checkpoint,
        "predictions": processed,
    }
    with open(args.save_path, "w") as f:
        json.dump(data_dict, f, indent=4)
