"""
Perplexity script for Seq2Seq LM
"""
import json
import os
import sys

from argparse import ArgumentParser

from evaluate import load
from transformers import AutoTokenizer

# add narrativeDataset.py file to the path
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))
from narrative_dataset import narrative_dataset, highlight_preprocess_function

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--model", type=str, required=True)
    parser.add_argument("--checkpoint", type=str, required=False)
    parser.add_argument("--dataset", type=str, required=True)
    parser.add_argument("--prompt", type=str, default="{input}{question}")
    parser.add_argument(
        "--prompt_positive",
        type=str,
        default="{positive}",
        help="Prompt to format the hightlighted positive. Default is {positive}, which is no formatting",
    )
    parser.add_argument(
        "--prompt_negative",
        type=str,
        default="{negative}",
        help="Prompt to format the hightlighted negative. Default is {negative}, which is no formatting",
    )
    parser.add_argument(
        "--save_path",
        type=str,
        default="perplexity.json",
        help="Path to save the results",
    )
    args = parser.parse_args()
    assert os.path.exists(args.dataset), "Dataset file not found."
    if args.checkpoint:
        assert os.path.exists(args.checkpoint), "Checkpoint not found."
    os.makedirs(os.path.dirname(args.save_path), exist_ok=True)
    perplexity = load(
        "finetuning/Seq2SeqLM/perplexitySeq2SeqLM.py", module_type="metric"
    )
    tokenizer = AutoTokenizer.from_pretrained(args.model)
    test = narrative_dataset(
        tokenizer,
        task_type="seq2seq",
        test_path=args.dataset,
        prompt=args.prompt,
        call_first=highlight_preprocess_function,
        fn_kwargs_call_first={
            "prompt_positive": args.prompt_positive,
            "prompt_negative": args.prompt_negative,
        },
    )["test"]
    ground = [l for l in test["question"]]
    context = [l for l in test["prompt_text"]]
    results = perplexity.compute(
        predictions=ground,
        context=context,
        add_start_token=False,
        model_id=args.model,
        checkpoint=args.checkpoint,
    )
    with open(args.save_path, "w") as f:
        json.dump(results, f, indent=4)
