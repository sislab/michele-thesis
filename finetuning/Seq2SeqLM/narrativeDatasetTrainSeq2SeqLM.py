"""
Training script for Seq2Seq LM
"""
import json
import os
import sys

from argparse import ArgumentParser

from transformers import (
    AutoModelForSeq2SeqLM,
    AutoTokenizer,
    DataCollatorForSeq2Seq,
    Seq2SeqTrainer,
    Seq2SeqTrainingArguments,
)

# add narrativeDataset.py file to the path
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))
from narrative_dataset import narrative_dataset, highlight_preprocess_function, token_balance_preprocess_function


def save_hyper(
    path: str,
    **kwargs: dict,
):
    """
    save hyperparameters to a json file
    """
    with open(os.path.join(path, "hyperparameters.json"), "w") as f:
        json.dump(kwargs, f, indent=4)


if __name__ == "__main__":
    parser = ArgumentParser(
        prog="Fine tuner for narrative dataset",
        description="Fine tune a model on the narrative dataset",
    )
    parser.add_argument(
        "--model",
        type=str,
        required=True,
        help="Model to use for fine tuning from huggingface.co/models",
    )
    # parser.add_argument("--tokenizer", type=str, required=False, help = "Tokenizer to use for fine tuning from huggingface.co/models. By default try to use the auto tokenizer if present")
    parser.add_argument(
        "--train", type=str, required=True, help="Path to the training dataset"
    )
    parser.add_argument(
        "--save_path",
        type=str,
        default="checkpoints",
        help="Path to save the checkpoints",
    )
    parser.add_argument(
        "--n_checkpoints", type=int, default=2, help="Number of checkpoints to keep"
    )
    parser.add_argument(
        "--test", type=str, required=True, help="Path to the test dataset"
    )
    parser.add_argument(
        "--block_size", type=int, default=512, help="Block size for the model"
    )
    parser.add_argument(
        "--lr", type=float, default=2e-5, help="Learning rate for the model"
    )
    parser.add_argument(
        "--wd", type=float, default=0.01, help="Weight decay for the model"
    )
    parser.add_argument(
        "--epochs", type=int, default=100, help="Number of epochs for the model"
    )
    parser.add_argument(
        '--batch_size', type=int, default=8, help = 'Batch size'
    )
    parser.add_argument(
        "--prompt",
        type=str,
        default="{narrative}",
        help="Prompt to add to the input text. Default is {narrative}. Model predicts the dataset question afterwards",
    )
    parser.add_argument(
        "--prompt_positive",
        type=str,
        default="{positive}",
        help="Prompt to format the hightlighted positive. Default is {positive}, which is no formatting",
    )
    parser.add_argument(
        "--prompt_negative",
        type=str,
        default="{negative}",
        help="Prompt to format the hightlighted negative. Default is {negative}, which is no formatting",
    )
    parser.add_argument(
        "--noise_tune",
        type=float,
        default=0.0,
        help="If present, fine tune the model with noise",
    )
    parser.add_argument('--rebalance',
                        type=bool, default=False, help='Rebalance the dataset by dropping 90% of the mi dispiace occurrences')
   
    args = parser.parse_args()

    assert os.path.exists(args.train), f"File {args.train} not found"
    assert os.path.isfile(args.train), f"Path {args.train} is not a file"
    assert os.path.exists(args.test), f"File {args.test} not found"
    assert os.path.isfile(args.test), f"Path {args.test} is not a file"
    os.makedirs(args.save_path, exist_ok=True)

    tokenizer = AutoTokenizer.from_pretrained(args.model)

    lm_splitted = narrative_dataset(
        tokenizer=tokenizer,
        task_type="seq2seq",
        train_path=args.train,
        test_path=args.test,
        prompt=args.prompt,
        batch_size=args.batch_size,
        block_size=args.block_size,
        call_first=highlight_preprocess_function,
        fn_kwargs_call_first={
            "prompt_positive": args.prompt_positive,
            "prompt_negative": args.prompt_negative,
        },
        call_back= token_balance_preprocess_function if args.rebalance else None
    )

    data_collator = DataCollatorForSeq2Seq(tokenizer=tokenizer, model=args.model)
    # try to load the model
    model = AutoModelForSeq2SeqLM.from_pretrained(args.model)

    training_args = Seq2SeqTrainingArguments(
        output_dir=args.save_path,
        evaluation_strategy="epoch",
        learning_rate=args.lr,
        weight_decay=args.wd,
        push_to_hub=False,
        save_strategy="epoch",
        report_to="wandb",
        num_train_epochs=args.epochs,
        save_total_limit=args.n_checkpoints,
        neftune_noise_alpha=args.noise_tune,
        per_device_train_batch_size=args.batch_size,
        per_device_eval_batch_size=args.batch_size
    )
    save_hyper(args.save_path, **vars(args))
    trainer = Seq2SeqTrainer(
        model=model,
        args=training_args,
        train_dataset=lm_splitted["train"],
        eval_dataset=lm_splitted["test"],
        data_collator=data_collator,
    )

    trainer.train()
