import json
import os
import sys
from argparse import ArgumentParser
from transformers import AutoTokenizer
import evaluate

# add narrativeDataset.py file to the path
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))
from narrative_dataset import (
    narrative_dataset_raw,
    narrative_dataset,
    narrative_dataset_untokenized,
    highlight_preprocess_function,
)


if __name__ == "__main__":
    argparser = ArgumentParser()
    argparser.add_argument("--dataset", type=str, required=True)
    args = argparser.parse_args()
    assert os.path.exists(args.dataset), "Ground truth file not found."
    tokenizer = AutoTokenizer.from_pretrained("gsarti/it5-small")
    test_raw = narrative_dataset_raw(test_path=args.dataset)["test"]
    test_untokenized = narrative_dataset_untokenized(test_path=args.dataset)["test"]
    test_dataset = narrative_dataset(
        tokenizer=tokenizer, test_path=args.dataset, task_type="causal"
    )["test"]
    test_call_first = narrative_dataset(
        tokenizer=tokenizer,
        test_path=args.dataset,
        task_type="causal",
        call_first=highlight_preprocess_function,
        fn_kwargs_call_first={
            "prompt_positive": "[VERDE]({positive})",
            "prompt_negative": "[ROSSO]({negative})",
        },
    )["test"]
    import pdb

    pdb.set_trace()
