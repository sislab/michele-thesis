"""
Training script for Causal LM using Optuna and a Config File
"""
import json
import os
import sys
import shutil
import tomllib

from argparse import ArgumentParser

from transformers import (
    AutoModelForCausalLM,
    AutoTokenizer,
    DataCollatorForLanguageModeling,
    Trainer,
    TrainingArguments,
)

# add narrativeDataset.py file to the path
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))
from narrative_dataset import narrative_dataset, highlight_preprocess_function


def save_hyper(
    path: str,
    **kwargs: dict,
):
    """
    save hyperparameters to a json file
    """
    with open(os.path.join(path, "hyperparameters.json"), "w") as f:
        json.dump(kwargs, f, indent=4)


def model_init(trial):
    return AutoModelForCausalLM.from_pretrained(args.model)


if __name__ == "__main__":
    parser = ArgumentParser(
        prog="Fine tuner for narrative dataset",
        description="Fine tune a model on the narrative dataset",
    )
    parser.add_argument(
        "--config",
        type=str,
        default="config.toml",
        help="If present, load the config from a toml file",
    )

    args = parser.parse_args()
    # load config
    with open(args.config, "rb") as f:
        config = tomllib.load(f)
    # load parameters
    model = config["model"]["name"]
    train_path = config["dataset"]["train_path"]
    test_path = config["dataset"]["test_path"]
    save_path = config["save"]["save_path"]
    n_checkpoints = config["save"]["n_checkpoints"]
    prompt = config["hyperparameters"]["prompt"]
    epochs = config["hyperparameters"]["epochs"]
    trials = config["hyperparameters"]["trials"]
    prompt_positive = config["hyperparameters"]["prompt_positive"]
    prompt_negative = config["hyperparameters"]["prompt_negative"]
    block_size = config["hyperparameters"]["block_size"]
    # load optuna hyperparameter dict
    optuna = config["optuna"]
    # assert existence of files and create output directory
    assert os.path.exists(train_path), f"File {train_path} not found"
    assert os.path.isfile(train_path), f"Path {train_path} is not a file"
    assert os.path.exists(test_path), f"File {test_path} not found"
    assert os.path.isfile(test_path), f"Path {test_path} is not a file"
    os.makedirs(save_path, exist_ok=True)
    # copy the used ocnfig
    shutil.copyfile(args.config, os.path.join(save_path, "config.toml"))
    # load the tokenizer
    tokenizer = AutoTokenizer.from_pretrained(model)
    # laod the dataset
    lm_splitted = narrative_dataset(
        tokenizer=tokenizer,
        task_type="causal",
        train_path=train_path,
        test_path=test_path,
        prompt=prompt,
        block_size=block_size,
        call_first=highlight_preprocess_function,
        fn_kwargs_call_first={
            "prompt_positive": prompt_positive,
            "prompt_negative": prompt_negative,
        },
    )
    # data collator and traiing args
    data_collator = DataCollatorForLanguageModeling(tokenizer=tokenizer, mlm=False)
    training_args = TrainingArguments(
        output_dir=save_path,
        evaluation_strategy="epoch",
        push_to_hub=False,
        save_strategy="epoch",
        report_to="wandb",
        num_train_epochs=epochs,
        save_total_limit=n_checkpoints,
    )
    # create the trainer
    trainer = Trainer(
        model=None,
        args=training_args,
        train_dataset=lm_splitted["train"],
        eval_dataset=lm_splitted["test"],
        data_collator=data_collator,
        model_init=lambda: AutoModelForCausalLM.from_pretrained(model),
    )

    # optuna trials setup from dict
    def optuna_hp_space(trial):
        def switching(type: str, name: str, args: list):
            if type == "float":
                return trial.suggest_float(name, args[0], args[1], log=True)
            elif type == "int":
                return trial.suggest_int(name, args[0], args[1], log=True)
            elif type == "categorical":
                return trial.suggest_categorical(name, *args)
            else:
                raise ValueError(f"Type {type} not recognized")

        mapped = {k: switching(v[-1], k, v[0:-1]) for k, v in optuna.items()}
        return mapped

    # optiuna hyperparameter search
    best_runs = trainer.hyperparameter_search(
        direction="minimize",
        backend="optuna",
        hp_space=optuna_hp_space,
        n_trials=trials,
    )
    # save best
    with open(os.path.join(save_path, "best_hyperparameters.json"), "w") as f:
        json.dump(best_runs, f, indent=4)
