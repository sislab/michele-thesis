from transformers import AutoTokenizer, AutoModelForSequenceClassification

import os
import sys
import json
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(SCRIPT_DIR)
from narrative_dataset import narrative_dataset, highlight_preprocess_function, narrative_dataset_raw, narrative_dataset_untokenized
from sentiment_analyzer_api import SentimentAnalyzerHF
from argparse import ArgumentParser
import torch

if torch.cuda.is_available():
    device = "cuda"
elif torch.backends.mps.is_available():
    device = "mps"
else:
    device = "cpu"
if __name__ == '__main__':

    parser = ArgumentParser()
    parser.add_argument('--inference', type=str, required=True)
    # parser.add_argument('--prompt', type=str, required=True)
    parser.add_argument('--save_path', type=str, required=True)
    args = parser.parse_args()
    os.makedirs(os.path.dirname(args.save_path), exist_ok=True)
    assert os.path.exists(args.inference)
    
    tokenizer = AutoTokenizer.from_pretrained("m-polignano-uniba/bert_uncased_L-12_H-768_A-12_italian_alb3rt0")
    tokenizer.model_max_length = 128

    model = AutoModelForSequenceClassification.from_pretrained('SISLab/amc-opt-msmd', trust_remote_code=True)
    
    with open(args.inference) as f:
        data = json.load(f)['predictions']

    sa = SentimentAnalyzerHF(model, tokenizer, device=device)
    results = {}
    results['test_predictions'] = sa(data)
    with open(args.save_path, 'w') as f:
        json.dump(results, f, indent=4)
    # outuput: ["positive", "negative", "negative", "neutral"]
