import json
import os
from argparse import ArgumentParser
from typing import Iterable

import textual
from datasets import Dataset, DatasetDict
from textual.app import App, ComposeResult
from textual.containers import Container, Horizontal, ScrollableContainer
from textual.widget import Widget
from textual.widgets import Button, Footer, Header, Input, Static
from transformers import (
    AutoModel,
    AutoModelForCausalLM,
    AutoModelForSeq2SeqLM,
    AutoTokenizer,
    DataCollatorForLanguageModeling,
    Trainer,
    TrainingArguments,
    pipeline,
)


class MessageBox(Widget):
    def __init__(self, text: str, role: str) -> None:
        self.text = text
        self.role = role
        super().__init__()

    def compose(self) -> ComposeResult:
        yield Static(self.text, classes=f"message {self.role}")


class ChatApp(App):
    TITLE = "chatui"
    # SUB_TITLE = "LLM directly in your terminal"
    CSS_PATH = "styles.css"

    def __init__(self, pipe: pipeline) -> None:
        super().__init__()
        self.pipe = pipe

    def compose(self) -> ComposeResult:
        yield Header()
        yield ScrollableContainer(id="conversation_box")
        with Horizontal(id="input_box"):
            yield Input(placeholder="Enter your message", id="message_input")
            yield Button(label="Send", variant="success", id="send_button")
        yield Footer()

    async def on_button_pressed(self, button: Button):
        await self.process_conversation()

    async def on_input_submitted(self) -> None:
        await self.process_conversation()

    async def process_conversation(self) -> None:
        message_input = self.query_one("#message_input", Input)
        # Don't do anything if input is empty
        if message_input.value == "":
            return
        button = self.query_one("#send_button")
        conversation_box = self.query_one("#conversation_box")
        # disable the input and button while we process the input
        self.toggle_widgets(message_input, button)
        input_text = message_input.value.strip()
        message_box = MessageBox(input_text, "question")
        conversation_box.mount(message_box)
        conversation_box.scroll_end(animate=True)
        # Clean up the input without triggering events
        with message_input.prevent(Input.Changed):
            message_input.value = ""

        response = self.pipe(input_text)

        conversation_box.mount(MessageBox(response[0]["generated_text"], "Answer"))
        # re-enable the input and button
        self.toggle_widgets(message_input, button)
        # scroll to the end of the conversation
        conversation_box.scroll_end(animate=True)
        # focus the input so we can type again
        message_input.focus()

    def toggle_widgets(self, *widgets: Widget) -> None:
        for w in widgets:
            w.disabled = not w.disabled


import warnings

if __name__ == "__main__":
    parser = ArgumentParser(prog="Runner for LLMs", description="Run a model")
    parser.add_argument(
        "--model",
        type=str,
        required=True,
        help="Model to use for fine tuning from huggingface.co/models",
    )
    parser.add_argument(
        "--checkpoint",
        type=str,
        help="Path to load the checkpoints if any, otherwise use the pre-trained model",
    )
    # parser.add_argument("--prompt", type=str, default="" ,help = "Prompt to add to the full text")
    # parser.add_argument("--block_size", type=int, default=128 ,help = "Block size for the model")

    args = parser.parse_args()

    # if checkpoint is provided check if it exists
    if args.checkpoint:
        assert os.path.exists(args.checkpoint), f"File {args.checkpoint} not found"
        assert os.path.isdir(args.checkpoint), f"Path {args.checkpoint} is not a file"
    else:
        warnings.warn("No checkpoint provided, using base pre-trained model instead")

    tokenizer = AutoTokenizer.from_pretrained(args.model)
    tokenizer.padding_side = "left"

    try:
        model = AutoModelForCausalLM.from_pretrained(
            args.checkpoint if args.checkpoint else args.model
        )
        pipe = pipeline(
            task="text-generation",
            model=model,
            tokenizer=tokenizer,
            max_length=50,
            return_full_text=False,
        )
    except ValueError:
        model = AutoModelForSeq2SeqLM.from_pretrained(
            args.checkpoint if args.checkpoint else args.model
        )
        pipe = pipeline(
            task="text2text-generation", model=model, tokenizer=tokenizer, max_length=50
        )

    if args.checkpoint:
        ChatApp.SUB_TITLE = (
            f"Fine-tuned {args.model} using checkpoint {args.checkpoint}"
        )
    else:
        ChatApp.SUB_TITLE = f"Pre-trained {args.model}"
    app = ChatApp(pipe)
    app.run()
