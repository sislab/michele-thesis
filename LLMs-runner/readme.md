# Runner


this folder contains data to run ChatGPT and huggingface LLMs

#### create env by
```bash
conda env create -f environment.yml
``` 

#### activate env by
```bash
conda activate llms
``` 
#### CUDA
You might need to re-install your torch libraries according to the platform you are using
```
pip3 install torch torchvision torchaudio
# or
pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118
# or
pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu121
```
#### run code by customizing things similar to this

Mind that narrative elicitation input formats are different from the story cloze test input data format

```bash
python LLMs-runner/runner/json_format/chatGPT/ChatGPT.py -c LLMs-runner/runner/json_format/chatGPT/config/no_color/no_guidelines/chatGPT_config_0_shot.toml # code to run the narrative elicitation
python LLMs-runner/runner/text_format/chatGPT/ChatGPT.py -c LLMs-runner/runner/txt_format/chatGPT/config/chatGPT_config_1.toml # 
python LLMs-runner/runner/json_format/huggingface/runner.py -c LLMs-runner/runner/json_format/huggingface/config/with_color/no_guidelines/config_1_shot.toml
python LLMs-runner/runner/txt_format/huggingface/runner.py -c LLMs-runner/runner/txt_format/huggingface/config/config_2.toml

``` 
