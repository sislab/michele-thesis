# script that extracts the prompt used in each shot to make sure they are correct
import json
import sys
import os


if __name__ == "__main__":
    assert os.path.isdir(
        sys.argv[1]
    ), "Please provide a valid path to the results folder"
    files = os.listdir(sys.argv[1])
    files = [os.path.join(sys.argv[1], f) for f in files]
    for file in files:
        with open(file, "rb") as f:
        
            print (file)
            data = json.load(f)
        for model in data:
            for res in model["results"]:
                for output in res["output"]:
                    txt :str = output["generated_text"] 
                    txt = txt.split(res["input"])[-1]
                    # txt = txt.replace("'", "")
                    txt = txt.replace('fine:', "")
                    txt = txt.strip()
                    output["generated_text"] = txt
        with open(file, "w") as f:
            json.dump(data, f, indent=4)