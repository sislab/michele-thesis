# script that extracts the prompt used in each shot to make sure they are correct
import json
import sys
import os


if __name__ == "__main__":
    assert os.path.isfile(
        sys.argv[1]
    ), "Please provide a valid path to the results file"
    assert os.path.isfile(
        sys.argv[2]
    ), "Please provide a valid path to the results file"
    file = sys.argv[1]
    file2 = sys.argv[2]
    with open(file, "rb") as f:
        print (file)
        data = json.load(f)
    with open(file2, "rb") as f:
        print (file2)
        data2 = json.load(f)
    for model,cpy in zip(data, data2):
        for res,res_cpy in zip(model["results"], cpy["results"]):
            old = res["input"]
            res["input"] = res_cpy["input"]
            for output in res["output"]:
                # txt : str = output["generated_text"].replace('\t',' ').replace(old, res_cpy["input"])
                
                txt :str = output["generated_text"] 
                txt = txt.split(res["input"])[-1]
                # txt = txt.replace("'", "")
                txt = txt.replace('fine:', "")
                txt = txt.strip()
                output["generated_text"] = txt
    with open(file, "w") as f:
        json.dump(data, f, indent=4)