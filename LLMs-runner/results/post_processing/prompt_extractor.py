# script that extracts the prompt used in each shot to make sure they are correct
import json
import sys
import os


if __name__ == "__main__":
    assert os.path.isdir(
        sys.argv[1]
    ), "Please provide a valid path to the results folder"
    colors = os.listdir(sys.argv[1])
    colors = [os.path.join(sys.argv[1], f) for f in colors]
    for color in colors:
        guides = os.listdir(color)
        guides = [os.path.join(color, f) for f in guides]
        for guide in guides:
            shots = os.listdir(guide)
            shots = [os.path.join(guide, f) for f in shots]
            for shot in shots:
                files = os.listdir(shot)
                files = [os.path.join(shot, f) for f in files]
                prompt = set()
                for file in files:
                    with open(file, "rb") as f:
                        data = json.load(f)
                    for model in data:
                        for res in model["results"]:
                            for output in res["output"]:
                                txt = output["generated_text"]
                                txt = txt.replace(res["input"], "{prompt}")
                                prompt.add(txt.split("{prompt}")[0])
                for p in prompt:
                    print(f"file {shot} uses this prompt {p}")
