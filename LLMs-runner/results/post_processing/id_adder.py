import os
import sys
import json

if __name__ == "__main__":
    with open(sys.argv[1], "rb") as f:
        data = json.load(f)
    ids = [item["id"] for item in data]
    print(ids)
    # list all files recursively
    if os.path.isfile(sys.argv[2]):
        file_path = sys.argv[2]
        print(file_path)
        try:
            if file_path.endswith(".json"):
                with open(file_path, "rb") as f:
                    data = json.load(f)
                res = data[0]["results"]
                for item, id in zip(res, ids):
                    item["id"] = id
                data[0]["results"] = res
                with open(file_path, "w") as f:
                    json.dump(data, f, indent=4)
        except Exception as e:
            print(e)
            print(file_path)
            exit(1)
    for root, dirs, files in os.walk(sys.argv[2]):
        for file in files:
            file_path = os.path.join(root, file)
            print(file_path)
            try:
                if file_path.endswith(".json"):
                    with open(file_path, "rb") as f:
                        data = json.load(f)
                    res = data[0]["results"]
                    for item, id in zip(res, ids):
                        item["id"] = id
                    data[0]["results"] = res
                    with open(file_path, "w") as f:
                        json.dump(data, f, indent=4)
            except Exception as e:
                print(e)
                print(file_path)
                exit(1)
