# script that separates the json file into multiple json files
import os
import sys
import json

if __name__ == "__main__":
    assert len(sys.argv) == 2, f"Usage: python {os.path.basename(__file__)} input_file"
    input_file = sys.argv[1]
    assert os.path.exists(input_file), f"Input file {input_file} does not exist"
    with open(input_file, "r") as f:
        data = json.load(f)
    for item in data:
        name = item["model_name"]
        name = name.split("/")[-1]
        with open(name + ".json", "w") as f:
            json.dump([item], f, indent=4)
