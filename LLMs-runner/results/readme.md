folder containing results in json format
each file is named after the model is run.

There are three sets of results

- old dataset uses a machine translated from english to italian dataset from roc stories best 50
- cleaned dataset uses a machine translated from english to italian dataset from roc stories best 50. This dataset is also human corrected for better translation
- narrative elicitation. Uses datasets from the test set from Coadapt which is already a narrative dataset. However since it structured in a way to answer questions, those narratives were manually corrected in a way to make them more human readable and at the same time anonymising the data.
