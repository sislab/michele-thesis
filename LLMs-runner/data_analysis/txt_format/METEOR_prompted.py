import json
import sys
import os
import evaluate


ngram = 4
meteor = evaluate.load("meteor")

with open(sys.argv[1], "r") as f:
    sentences = f.readlines()
    for idx, s in enumerate(sentences):
        sentences[idx] = s.strip()
        sentences[idx] = sentences[idx].split(".")
        sentences[idx] = [s.strip() for s in sentences[idx] if s.strip() != ""]
print("METEOR scores for 5 sentences ")
for file in os.listdir(sys.argv[2]):
    with open(os.path.join(sys.argv[2], file), "r") as f:
        data = json.load(f)
    meteor_tot = 0
    for res, ref in zip(data[0]["results"], sentences):
        references = [". ".join(ref)]
        for response in res["output"]:
            df = response["generated_text"].split(":", 1)[1]  # responses
            df = df.replace("'", "", 1)
            df = df.split(".")
            try:
                df[4] = df[4].replace("'", "", 1)
            except IndexError:
                df[3] = df[3].replace("'", "", 1)
            df = [s.strip() for s in df if s.strip() != ""]
            df = df[:5]
            df = ". ".join(df)
            df = df.replace("\n", " ")
            df = df.replace("\t", " ")
            # import pdb; pdb.set_trace()
            meteor_tot += meteor.compute(predictions=[df], references=[references])[
                "meteor"
            ]
    print(
        f" model {data[0]['model_name']} average METEOR {ngram} score {meteor_tot / (len(data[0]['results'][0]['output'])*len(data[0]['results']))}"
    )
meteor_tot = 0
for res, ref in zip(data[0]["results"], sentences):
    references = [". ".join(ref)]
    pred = ". ".join(ref[:4])
    meteor_tot += meteor.compute(predictions=[pred], references=[references])["meteor"]
print(
    f" No answer average METEOR {ngram} score {meteor_tot / (len(data[0]['results']))}"
)
meteor_tot = 0
for res, ref in zip(data[0]["results"], sentences):
    references = [". ".join(ref)]
    pred = ". ".join(ref)
    meteor_tot += meteor.compute(predictions=[pred], references=[references])["meteor"]
print(f" Perfect average METEOR {ngram} score {meteor_tot / (len(data[0]['results']))}")

print("METEOR scores for only the last  5th sentence ")

for file in os.listdir(sys.argv[2]):
    with open(os.path.join(sys.argv[2], file), "r") as f:
        data = json.load(f)
    meteor_tot = 0
    for res, ref in zip(data[0]["results"], sentences):
        references = [ref[-1] + "."]

        for response in res["output"]:
            df = response["generated_text"].split(":", 1)[1]  # responses
            df = df.split("fine:")[-1].strip()
            df = df.replace("\n", " ")
            df = df.replace("\t", " ")
            # df = df.split('.')
            df = df.split(".")[0]
            df = [s.strip() for s in df if s.strip() != ""]
            df = ". ".join(df)
            # import pdb; pdb.set_trace()
            meteor_tot += meteor.compute(predictions=[df], references=[references])[
                "meteor"
            ]
    print(
        f" model {data[0]['model_name']} average METEOR {ngram} score {meteor_tot / (len(data[0]['results'][0]['output'])*len(data[0]['results']))}"
    )
