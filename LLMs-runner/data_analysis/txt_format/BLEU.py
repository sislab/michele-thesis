import json
import sys
import os
from torchmetrics.text import BLEUScore

ngram = 4
bleu = BLEUScore(n_gram=ngram)

with open(sys.argv[1], "r") as f:
    sentences = f.readlines()
    for idx, s in enumerate(sentences):
        sentences[idx] = s.strip()
        sentences[idx] = sentences[idx].split(".")
        sentences[idx] = [s.strip() for s in sentences[idx] if s.strip() != ""]
print("BLEU scores for 5 sentences ")
for file in os.listdir(sys.argv[2]):
    with open(os.path.join(sys.argv[2], file), "r") as f:
        data = json.load(f)
    bleu_tot = 0
    for res, ref in zip(data[0]["results"], sentences):
        references = [". ".join(ref)]
        for response in res["output"]:
            df = response["generated_text"].split(":", 1)[1]  # responses
            df = df.replace("'", "", 1)
            df = df.split(".")
            try:
                df[4] = df[4].replace("'", "", 1)
            except IndexError:
                df[3] = df[3].replace("'", "", 1)
            df = [s.strip() for s in df if s.strip() != ""]
            df = df[:5]
            df = ". ".join(df)
            df = df.replace("\n", " ")
            df = df.replace("\t", " ")

            bleu_tot += bleu([df], [references])
    print(
        f" model {data[0]['model_name']} average BLEU {ngram} score {bleu_tot / (len(data[0]['results'][0]['output'])*len(data[0]['results']))}"
    )
bleu_tot = 0
for res, ref in zip(data[0]["results"], sentences):
    references = [". ".join(ref)]
    pred = ". ".join(ref[:4])
    bleu_tot += bleu([pred], [references])
print(f" No answer average BLEU {ngram} score {bleu_tot / (len(data[0]['results']))}")
bleu_tot = 0
for res, ref in zip(data[0]["results"], sentences):
    references = [". ".join(ref)]
    pred = ". ".join(ref)
    bleu_tot += bleu([pred], [references])
print(f" Perfect average BLEU {ngram} score {bleu_tot / (len(data[0]['results']))}")

print("BLEU scores for only the last  5th sentence ")

for file in os.listdir(sys.argv[2]):
    with open(os.path.join(sys.argv[2], file), "r") as f:
        data = json.load(f)
    bleu_tot = 0
    for res, ref in zip(data[0]["results"], sentences):
        references = [ref[-1] + "."]

        for response in res["output"]:
            df = response["generated_text"].split(":", 1)[1]  # responses
            df = df.replace("'", "", 1)
            df = df.split(".")
            try:
                df[4] = df[4].replace("'", "", 1)
                four = True
            except IndexError:
                df[3] = df[3].replace("'", "", 1)
                four = False
            df = [s.strip() for s in df if s.strip() != ""]
            df = df[:5]
            df = ". ".join(df)
            df = df.replace("\n", " ")
            df = df.replace("\t", " ")
            df = df.split(".")

            if four:
                try:
                    df = df[4]
                except IndexError:
                    df = df[3]
            else:
                try:
                    df = df[3]
                except IndexError:
                    df = df[2]
            import pdb

            pdb.set_trace()
            bleu_tot += bleu([df], [references])
    print(
        f" model {data[0]['model_name']} average BLEU {ngram} score {bleu_tot / (len(data[0]['results'][0]['output'])*len(data[0]['results']))}"
    )
