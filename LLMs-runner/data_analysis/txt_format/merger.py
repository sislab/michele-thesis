import os
import sys
import json

if __name__ == "__main__":
    assert os.path.exists(sys.argv[1])
    assert os.path.exists(sys.argv[2])
    if os.path.exists(sys.argv[4]):
        os.remove(sys.argv[4])
    with open(sys.argv[1], "rb") as f:
        data1 = json.load(f)
    with open(sys.argv[2], "rb") as f:
        data2 = json.load(f)
    with open(sys.argv[3], "rb") as f:
        data3 = json.load(f)

    # merge
    total = {}
    for item in data1:
        if (
            item["model_name"] not in total.keys() and type(item["results"]) != str
        ):  # and  "error" not in item["results"].split(" "):
            total[item["model_name"]] = item["results"]
    for item in data2:
        if (
            item["model_name"] not in total.keys() and type(item["results"]) != str
        ):  # and  "error" not in item["results"].split(" "):
            total[item["model_name"]] = item["results"]
    for item in data3:
        if (
            item["model_name"] not in total.keys() and type(item["results"]) != str
        ):  # and  "error" not in item["results"].split(" "):
            total[item["model_name"]] = item["results"]
    with open(sys.argv[4], "w") as f:
        json.dump(total, f, indent=4)
