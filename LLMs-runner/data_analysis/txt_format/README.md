# LLMs-runner

create env by

```
conda env create -f environment.yml
conda activate llms
pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118
```

run by

```
pip install git+https://github.com/huggingface/transformers
cd LLLMs-runner
conda activate llms or give the path of the python inside the env
python roc_tester.py -c config.toml
```
