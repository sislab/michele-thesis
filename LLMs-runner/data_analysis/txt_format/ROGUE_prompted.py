import json
import sys
import os
from torchmetrics.text import ROUGEScore

# from pprint import pprint as print

rouge = ROUGEScore()


def dict_sum(a: dict, b: dict):
    c = {}
    for k, v in a.items():
        try:
            c[k] = v + b[k]
        except KeyError:
            c[k] = v
    return c


def divide_dict(a: dict, div: int):
    c = {}
    for k, v in a.items():
        c[k] = v / div
    return c


def average_dict(a: dict):
    c = 0
    for k, v in a.items():
        c += v
    return c / len(a)


with open(sys.argv[1], "r") as f:
    sentences = f.readlines()
    for idx, s in enumerate(sentences):
        sentences[idx] = s.strip()
        sentences[idx] = sentences[idx].split(".")
        sentences[idx] = [s.strip() for s in sentences[idx] if s.strip() != ""]

print("ROUGE scores for 5 sentences ")
for file in os.listdir(sys.argv[2]):
    with open(os.path.join(sys.argv[2], file), "r") as f:
        data = json.load(f)
    rouge_tot = {}
    for res, ref in zip(data[0]["results"], sentences):
        references = ". ".join(ref)
        for response in res["output"]:
            df = response["generated_text"].split(":", 1)[1]  # responses
            df = df.replace("'", "", 1)
            df = df.split(".")
            try:
                df[4] = df[4].replace("'", "", 1)
            except IndexError:
                df[3] = df[3].replace("'", "", 1)
            df = [s.strip() for s in df if s.strip() != ""]
            df = df[:5]
            df = ". ".join(df)
            df = df.replace("\n", " ")
            df = df.replace("\t", " ")

            rouge_tot = dict_sum(rouge(df, references), rouge_tot)
    print(
        f" model {data[0]['model_name']} average ROUGE score {average_dict(divide_dict(rouge_tot,(len(data[0]['results'][0]['output'])*len(data[0]['results']))))}"
    )
rouge_tot = {}
for res, ref in zip(data[0]["results"], sentences):
    references = ". ".join(ref)
    pred = ". ".join(ref[:4])
    rouge_tot = dict_sum(rouge(pred, references), rouge_tot)
print(
    f" No answer average ROUGE score {average_dict(divide_dict(rouge_tot ,(len(data[0]['results']))))}"
)
rouge_tot = {}
for res, ref in zip(data[0]["results"], sentences):
    references = ". ".join(ref)
    pred = ". ".join(ref)
    # import pdb; pdb.set_trace()
    rouge_tot = dict_sum(rouge(pred, references), rouge_tot)
print(
    f" Perfect average ROUGE score {average_dict(divide_dict(rouge_tot ,(len(data[0]['results']))))}"
)


print("ROGUE scores for only the last  5th sentence ")

for file in os.listdir(sys.argv[2]):
    with open(os.path.join(sys.argv[2], file), "r") as f:
        data = json.load(f)
    rouge_tot = {}
    for res, ref in zip(data[0]["results"], sentences):
        references = [ref[-1] + "."]
        for response in res["output"]:
            df = response["generated_text"].split(":", 1)[1]  # responses
            df = df.split("fine:")[-1].strip()
            df = df.replace("\n", " ")
            df = df.replace("\t", " ")
            # df = df.split('.')
            df = df.split(".")[0]
            df = [s.strip() for s in df if s.strip() != ""]
            df = ". ".join(df)
            # import pdb; pdb.set_trace()
            # import pdb; pdb.set_trace()
            rouge_tot = dict_sum(rouge(df, references), rouge_tot)
    # import pdb; pdb.set_trace()
    print(
        f" model {data[0]['model_name']} average ROUGE score {average_dict(divide_dict(rouge_tot,(len(data[0]['results'][0]['output'])*len(data[0]['results']))))}"
    )
