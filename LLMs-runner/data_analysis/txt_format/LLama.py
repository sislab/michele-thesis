import llama

tokenizer = llama.LLaMATokenizer.from_pretrained("decapoda-research/llama-7b-hf")
model = llama.LLaMAForCausalLM.from_pretrained("decapoda-research/llama-7b-hf")
print(
    tokenizer.decode(
        model.generate(tokenizer("Yo mama", return_tensors="pt")["input_ids"])[0]
    )
)
