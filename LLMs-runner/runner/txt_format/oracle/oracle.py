# script to generate oracle results from a given input file
# input file should be a txt file with each line containing a sequence of sentences
# the last sentence is the target sentence which is extracted and used as the output
try:
    import toml as tomllib

    imported = True
except ImportError:
    import tomllib

    imported = False
from argparse import ArgumentParser
import sys
import os
import json
import warnings
import tqdm

parser = ArgumentParser(
    "LLM runner",
    usage=f"python {os.path.basename(__file__)} -c config.toml",
    description="LLM tester",
)
parser.add_argument("-c", "--config", type=str, required=True)
args = parser.parse_args()
print(f"Loading config file {args.config}")

if not imported:
    with open(args.config, "rb") as f:
        config = tomllib.load(f)
else:
    config = tomllib.load(args.config)

input_file = config["input"]["input_file"]
output_file = config["output"]["output_file"]
condition = config["model"]["prompt"]

assert os.path.exists(input_file), f"Input file {input_file} does not exist"
if os.path.exists(output_file):
    warnings.warn(f"Output file {output_file} does already exist, replacing it")
    os.remove(output_file)

with open(input_file, "r") as f:
    sentences = f.readlines()
    sequences = [
        [t.strip() for t in item.strip().split(".") if t.strip() != ""]
        for item in sentences
    ]

output = list()

current = dict()
current["model_name"] = "oracle"
results = []
for idx, prompt in zip(tqdm.tqdm(sequences), sequences):
    pro = ". ".join(prompt[:-1])
    if not pro.endswith("."):
        pro += "."
    result = prompt[-1]
    if not result.endswith("."):
        result += "."
    dictionary_result = {
        "input": pro,
        "output": [{"generated_text": condition.format(prompt=pro) + " " + result}],
    }
    results.append(dictionary_result)
    # immediately write to file
current["results"] = results
output.append(current)
with open(output_file, "w") as f:
    json.dump(output, f, indent=4)
