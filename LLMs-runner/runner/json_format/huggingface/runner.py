# script to run LLMs on a prompt
from transformers import AutoTokenizer
import transformers
import torch

try:
    import toml as tomllib

    imported = True
except ImportError:
    import tomllib

    imported = False
from argparse import ArgumentParser
import sys
import os
import json
import warnings
import tqdm

parser = ArgumentParser(
    "LLM runner",
    usage=f"python {os.path.basename(__file__)} -c config.toml",
    description="LLM tester",
)
parser.add_argument("-c", "--config", type=str, required=True)
args = parser.parse_args()
print(f"Loading config file {args.config}")

if not imported:
    with open(args.config, "rb") as f:
        config = tomllib.load(f)
else:
    config = tomllib.load(args.config)

models = config["model"]["model_names"]
device = config["model"]["device"]
precision = config["model"]["precision"]
condition = config["model"]["prompt"]
input_file = config["input"]["input_file"]
color = config["model"]["color"]
output_file = config["output"]["output_file"]
if precision == 16:
    dtype = torch.bfloat16
elif precision == 32:
    dtype = torch.float32
else:
    print(f"Precision {precision} bits not supported")
    sys.exit(1)

assert os.path.exists(input_file), f"Input file {input_file} does not exist"
if os.path.exists(output_file):
    warnings.warn(f"Output file {output_file} does already exist, replacing it")
    os.remove(output_file)

if input_file.endswith(".json"):
    with open(input_file, "rb") as f:
        data = json.load(f)
    if color:
        sequences = []
        for item in data:
            text = item["text"]
            for positive in item["highlight_positive"]:
                text = text.replace(positive, f"[VERDE]({positive})")
            for negative in item["highlight_negative"]:
                text = text.replace(negative, f"[ROSSO]({negative})")
            sequences.append(text)
    else:
        sequences = [item["text"] for item in data]

model_results = []

for idx, model_name in zip(tqdm.tqdm(range(len(models))), models):
    try:
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        model = model_name
        output_sequences = []
        pipeline = transformers.pipeline(
            "text-generation",
            model=model,
            tokenizer=tokenizer,
            torch_dtype=dtype,
            trust_remote_code=True,
            device_map=device,
        )
        pbar = tqdm.tqdm(range(len(sequences)))
        for i in pbar:
            prompt = sequences[i]
            pbar.set_description_str(prompt)
            # import pdb; pdb.set_trace()
            res = pipeline(
                condition.format(prompt=prompt),
                # max_length=512,
                max_new_tokens=128,
                do_sample=True,
                top_k=10,
                num_return_sequences=1,
                eos_token_id=tokenizer.eos_token_id,
                return_full_text=False,
            )
            output_sequences.append(res)
        results = []
        for i, o in enumerate(output_sequences):
            prompt = sequences[i]
            results.append({"input": prompt, "output": o})
        model_results.append(
            {"model_name": model_name, "prompt": condition, "results": results}
        )
        # immediately write results
        with open(output_file, "w") as f:
            f.write(json.dumps(model_results, indent=4))
    except Exception as e:
        model_results.append(
            {
                "model_name": model_name,
                "results": f"error running inference on this model {e}",
            }
        )
        with open(output_file, "w") as f:
            f.write(json.dumps(model_results, indent=4))
    # free cache
    from huggingface_hub import scan_cache_dir

    if config["cache"]["delete_cache"]:
        ls = scan_cache_dir()
        hashes = []
        data = ls.__dict__
        repos = data["repos"]
        tot = len(tuple(repos))
        for i in range(tot):
            tup = tuple(repos)[i]
            revision = tup.__dict__["revisions"]
            commit = tuple(revision)[0]
            hash_no = commit.__dict__["commit_hash"]
            hashes.append(hash_no)
        delete_strategy = scan_cache_dir().delete_revisions(*hashes)
        print("Will free " + delete_strategy.expected_freed_size_str)
        delete_strategy.execute()
