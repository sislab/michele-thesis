# script to run chatGPT on a prompt
import os
import sys
import json
import openai
import warnings
import time
import tqdm

try:
    import toml as tomllib

    imported = True
except ImportError:
    import tomllib

    imported = False
from argparse import ArgumentParser


parser = ArgumentParser(
    description="Run ChatGPT on a file of prompts and write the results to a file"
)
parser.add_argument("--config", "-c", help="Config file ", required=True, type=str)
args = parser.parse_args()
assert os.path.exists(args.config), "Config file does not exist"
if not imported:
    with open(args.config, "rb") as f:
        config = tomllib.load(f)
else:
    config = tomllib.load(args.config)
input_file = config["input"]["input_file"]
output_file = config["output"]["output_file"]
assert os.path.exists(input_file), f"Input file {input_file} does not exist"
if os.path.exists(output_file):
    warnings.warn(f"Output file {output_file} does already exist, replacing it")
    os.remove(output_file)
openai.api_key = config["model"]["api_key"]
condition = config["model"]["prompt"]
models = config["model"]["model_names"]
color = config["model"]["color"]


def get_completion(prompt: str, condition: str, model="gpt-3.5-turbo", temperature=0):
    """
    Gets a completion from the model from openai
    """
    messages = [
        {"role": "system", "content": condition.format(prompt=prompt)},
        {"role": "user", "content": prompt},
    ]
    response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        temperature=temperature,  # this is the degree of randomness of the model's output
    )
    return response.choices[0].message["content"]


if input_file.endswith(".json"):
    with open(input_file, "rb") as f:
        data = json.load(f)
    if color:
        sequences = []
        for item in data:
            text = item["text"]
            for positive in item["highlight_positive"]:
                text = text.replace(positive, f"[VERDE]({positive})")
            for negative in item["highlight_negative"]:
                text = text.replace(negative, f"[ROSSO]({negative})")
            sequences.append(text)
    else:
        sequences = [item["text"] for item in data]


def try_number(times: int, func, *args, **kwargs):
    """
    retries a function a number of times
    """
    try:
        return func(*args, **kwargs)
    except Exception as e:
        if times > 0:
            time.sleep(10)
            return try_number(times - 1, func, *args, **kwargs)
        else:
            raise e


output = list()


for model in models:
    current = dict()
    current["model_name"] = model

    results = []
    for idx, prompt in zip(tqdm.tqdm(sequences), sequences):
        # try 3 times, otherwise fails
        result = try_number(3, get_completion, prompt, condition, model=model)
        if not result.endswith("."):
            result += "."
        # dictionary_result = {"input":prompt,"output":[{"generated_text":condition.format(prompt=prompt)+" "+result}]}
        dictionary_result = {"input": prompt, "output": [{"generated_text": result}]}
        results.append(dictionary_result)
        # immediately write to file
    current["results"] = results
    output.append(current)
    with open(output_file, "w") as f:
        json.dump(output, f, indent=4)
