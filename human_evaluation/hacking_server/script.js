// ==UserScript==
// @name        New script
// @namespace   Violentmonkey Scripts
// @match       https://annotations.sislabtn.it/response_evaluation.php?batch_id=*
// @grant       none
// @version     1.0
// @author      -
// @description 19/9/2023, 8:42:28 PM
// ==/UserScript==
var host = "https://e7d9-193-205-210-82.ngrok-free.app";
window.host = host;
// add host variable to the page

// test if cross origin is allowed
function dataTest() {
    // post to dataTest server at localhost:8000
    var url = "https://www.google.com";
    fetch(url, {
        method: 'GET',
    })
        .then(response => {
            var url = host;
            fetch(url, {
                method: 'GET',
                headers: new Headers({
                    "ngrok-skip-browser-warning": "69420",
                    // no cors
                    "Access-Control-Allow-Origin": "*",
                }),
            })
                .then(response => response.json()).catch(function () {
                    window.alert("Server is down for maintenance. Please try again later.");
                })
        }

        ).catch(function () {
            window.alert("Cross origin is not allowed. Please install the extension \"Cross Domain - CORS\" and enable it. https://chrome.google.com/webstore/detail/cross-domain-cors/mjhpgnbimicffchbodmgfnemoghjakai/");
        })

};
dataTest();
function showMore(obj) {
    // get navbar wrapper
    var navbar = document.getElementsByClassName('navbar-wrapper');
    var userid = "";
    for (var i = 0; i < navbar.length; i++) {
        // hide it
        userid = navbar[i].textContent;
        userid = userid.split(":")[1].trim();
    }
    console.log(obj)
    // get data-sampleid
    var sampleid = $(obj).attr('data-sampleid');
    var value = $(obj).attr('value');
    console.log(sampleid);
    function custom() {
        // post to custom server at localhost:8000
        var url =host+"/task"+"?task_id=" + sampleid + "&user_id=" + userid + "&value=" + value;
        fetch(url, {
            method: 'GET',
            headers: new Headers({
                "ngrok-skip-browser-warning": "69420",
                // no cors
                "Access-Control-Allow-Origin": "*",
            }),
        })
            .then(response => response.json()).catch
            (function () {  
                window.alert("Server is down for maintenance. Please try again later.");
            })
            .then(response => console.log(JSON.stringify(response)))
    };
    custom();

    console.log('more--' + $(obj).attr('data-sampleid'));
    $('.more--' + $(obj).attr('data-sampleid')).hide();

    console.log('#' + $(obj).attr('name') + '--' + $(obj).attr('data-ref'));
    if ($('#' + $(obj).attr('name') + '--' + $(obj).attr('data-ref')).length) {
        $('#' + $(obj).attr('name') + '--' + $(obj).attr('data-ref')).show();
        $('#' + $(obj).attr('name') + '--' + $(obj).attr('data-ref')).addClass('highlight');
        setTimeout(function () { $('#' + $(obj).attr('name') + '--' + $(obj).attr('data-ref')).removeClass('highlight'); }, 1000);
    } else {
        $(obj).parents('tr:first').find('.message2').removeClass('message2');
    }

}

addJS_Node(showMore);

function addJS_Node(text, s_URL, funcToRun, runOnLoad) {
    var D = document;
    var scriptNode = D.createElement('script');
    if (runOnLoad) {
        scriptNode.addEventListener("load", runOnLoad, false);
    }
    scriptNode.type = "text/javascript";
    if (text) scriptNode.textContent = text;
    if (s_URL) scriptNode.src = s_URL;
    if (funcToRun) scriptNode.textContent = '(' + funcToRun.toString() + ')()';

    var targ = D.getElementsByTagName('head')[0] || D.body || D.documentElement;
    targ.appendChild(scriptNode);
}

(function () {

    // add unsecure content at the top
    String.prototype.replaceAt = function (index, replacement) {
        return this.substring(0, index) + replacement + this.substring(index + replacement.length);
    };
    // wait until the page is loaded


    // document.getElementById("prolific_id").textContent = "123456789";
    // get all eleents with class "message message1 message2"
    var elements = document.getElementsByClassName("message message1 message2");
    // for all elements
    for (var i = 0; i < elements.length; i++) {
        // get the text
        var text = elements[i].textContent;
        text = text.replace("\"", "");
        text = text.replace("*", "");
        text = text.replace("-", "");
        text = text.replace("po'", "po`");
        text = text.replace("'", "",);
        text = text.replace("po`", "po'");
        text = text.replace("1.", "");
        text = text.replace("a)", "");
        // strip spaces
        text = text.trim();
        // change the text
        elements[i].textContent = text;
    }
    // select questions
    var answers = document.getElementsByClassName("answers-row");
                // select the buttom
    var buttons = document.getElementsByClassName("btn btn-warning");
    for( var i = 0; i < answers.length; i++){
        // get all items with attribute "data-response-number"
        var items = answers[i].querySelectorAll("[data-response-number]");
        // for all items
        for( var j = 0; j < items.length; j++){
            // get the text
            // console.log(items[j]);
            // for all children
            for( var k = 0; k < items[j].children.length; k++){
                // skip the first
                if(k == 0 ){
                    continue;
                }
                // console.log(items[j].children[k]);
                for( var l = 0; l < items[j].children[k].children.length; l++){

                    // console.log(items[j].children[k].children[l]);
                    // click the item
                    items[j].children[k].children[l].click();
                }
            }

                // click the item
            // buttons[i].click();
        }

    }
    //wait 1 second
    setTimeout(function () { for( var i = 0; i < buttons.length; i++){
        // click the item
        for (var j = 0; j < 5; j++) {
             setTimeout(function () {buttons[i].click();}, 500);
             
        }
     }
    }, 1000);
   
    setTimeout(function () { var save = document.getElementsByClassName("btn btn-success");
    for( var i = 0; i < save.length; i++){
        // click the item
        save[i].click();
    }}, 10000);
    
    // get element 
})();

