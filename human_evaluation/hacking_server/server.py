import json
import uvicorn
from argparse import ArgumentParser
from typing import Annotated
import random
from fastapi import Cookie, FastAPI, Form
from fastapi.responses import HTMLResponse, JSONResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from datetime import datetime
import tomllib


parser = ArgumentParser(description="server for crowdsourcing")
parser.add_argument("--config", type=str, default="config.toml", help="config file")

args, unknown = parser.parse_known_args()

with open(args.config, "rb") as f:
    config = tomllib.load(f)
host = config["server"]["host"]
port = config["server"]["port"]
path = config["server"]["path"]

app = FastAPI()

@app.options("/")
@app.post("/")
@app.get("/")
async def root():
    print ("root")
    return JSONResponse({"message": "Hello World"})
@app.options("/task")
# async def root():
#     print ("options")
#     return JSONResponse({"message": "Hello World"})
@app.post("/task")
@app.get("/task")
# @app.options("/task")
async def root(task_id: str, user_id: str, value: str):
    print ("task")
    "task_1-batch0-user3-47--model_name:[tiiuae/falcon-7b]_narrative_id:[47]"
    task_number = task_id.split("-")[0]
    batch_number = task_id.split("-")[1]
    user = task_id.split("-")[2] + "-" + user_id
    narrative_id = task_id.split("-")[3]
    model_name = task_id.split("model_name:[")[1].split("]")[0]

    try:
        with open(path, "r") as f:
            data = json.load(f)
    except FileNotFoundError as e:
        data = {}
        with open(path, "w") as f:
            json.dump(data, f, indent=4)
    try:
        data[task_number][batch_number][user][narrative_id][model_name]["value"] = value
    except KeyError as e:
        try:
            data[task_number][batch_number][user][narrative_id][model_name] = {"value": value}
        except KeyError as e:
            try:
                data[task_number][batch_number][user][narrative_id] = {model_name: {"value": value}}
            except KeyError as e:
                try:
                    data[task_number][batch_number][user] = {narrative_id: {model_name: {"value": value}}}
                except KeyError as e:
                    try:
                        data[task_number][batch_number] = {user: {narrative_id: {model_name: {"value": value}}}}
                    except KeyError as e:
                        try:
                            data[task_number] = {batch_number: {user: {narrative_id: {model_name: {"value": value}}}}}
                        except KeyError as e:
                            data = {task_number: {batch_number: {user: {narrative_id: {model_name: {"value": value}}}}}}
    with open(path, "w") as f:
        json.dump(data, f, indent=4)
    return JSONResponse({"message": "Hello World"})


if __name__ == "__main__":
    uvicorn.run(app, host=host, port=port)
