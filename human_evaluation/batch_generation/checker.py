import json
import os
import sys
import tomllib
import re
import pandas as pd
import evaluate
from argparse import ArgumentParser
import random

if __name__ == "__main__":
    # list folders
    folders = [ f for f in os.listdir(sys.argv[1]) if os.path.isdir(os.path.join(sys.argv[1],f)) ]
    print (folders)
    for folder in folders:
        path = os.path.join(sys.argv[1],folder)
        batches = [ f for f in os.listdir(path) if os.path.isdir(os.path.join(path,f)) ]
        batches.sort(key = lambda x: int(x))
        print (batches)
        for batch in batches:
            batch_path = os.path.join(path,batch)
            items = [ f for f in os.listdir(batch_path) if os.path.isfile(os.path.join(batch_path,f)) ]
            items.sort(key = lambda x: int(x.split('.')[0]))
            print (items)