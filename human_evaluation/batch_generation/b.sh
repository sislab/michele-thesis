rm -r '/Users/micheleyin/Documents/Elicitation/human_evaluation/users/'
mkdir /Users/micheleyin/Documents/Elicitation/human_evaluation/users/
mkdir /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user1
mkdir /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user2
mkdir /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user3

rm '/Users/micheleyin/Documents/Elicitation/human_evaluation/blacklist.json'

python '/Users/micheleyin/Documents/Elicitation/human_evaluation/black_list.py' '/Users/micheleyin/Documents/Elicitation/human_evaluation/users' '/Users/micheleyin/Documents/Elicitation/human_evaluation/blacklist.json'  

# first batch
python '/Users/micheleyin/Documents/Elicitation/human_evaluation/response_generation.py' -c '/Users/micheleyin/Documents/Elicitation/human_evaluation/config_shared.toml'

# black list
python '/Users/micheleyin/Documents/Elicitation/human_evaluation/black_list.py' '/Users/micheleyin/Documents/Elicitation/human_evaluation/users' '/Users/micheleyin/Documents/Elicitation/human_evaluation/blacklist.json'  

# copy batch 0 to user1
cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/0 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user1/0
# copy batch 1 to user2
cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/1 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user2/0
# copy batch 2 to user3
cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/2 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user3/0

# shared batches 
# update blacklist
python '/Users/micheleyin/Documents/Elicitation/human_evaluation/black_list.py' '/Users/micheleyin/Documents/Elicitation/human_evaluation/users' '/Users/micheleyin/Documents/Elicitation/human_evaluation/blacklist.json'

python '/Users/micheleyin/Documents/Elicitation/human_evaluation/response_generation.py' -c '/Users/micheleyin/Documents/Elicitation/human_evaluation/config_mid.toml'

# copy batch 0 to user1
cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/0 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user1/1
# copy batch 1 to user2
cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/1 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user2/1
# copy batch 2 to user3
cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/2 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user3/1

# shared batches 
# update blacklist
python '/Users/micheleyin/Documents/Elicitation/human_evaluation/black_list.py' '/Users/micheleyin/Documents/Elicitation/human_evaluation/users' '/Users/micheleyin/Documents/Elicitation/human_evaluation/blacklist.json'

python '/Users/micheleyin/Documents/Elicitation/human_evaluation/response_generation.py' -c '/Users/micheleyin/Documents/Elicitation/human_evaluation/config_mid.toml'

# copy batch 0 to user1
cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/0 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user1/2
# copy batch 1 to user2
cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/1 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user2/2
# copy batch 2 to user3
cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/2 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user3/2

# update blacklist
python '/Users/micheleyin/Documents/Elicitation/human_evaluation/black_list.py' '/Users/micheleyin/Documents/Elicitation/human_evaluation/users' '/Users/micheleyin/Documents/Elicitation/human_evaluation/blacklist.json'

python '/Users/micheleyin/Documents/Elicitation/human_evaluation/response_generation.py' -c '/Users/micheleyin/Documents/Elicitation/human_evaluation/config_final.toml'


# copy batch 0 to user1
cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/0 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user1/3
# copy batch 1 to user2
cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/1 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user2/3
# copy batch 2 to user3
cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/2 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user3/3

python '/Users/micheleyin/Documents/Elicitation/human_evaluation/black_list.py' '/Users/micheleyin/Documents/Elicitation/human_evaluation/users' '/Users/micheleyin/Documents/Elicitation/human_evaluation/blacklist.json'


python '/Users/micheleyin/Documents/Elicitation/human_evaluation/response_generation.py' -c '/Users/micheleyin/Documents/Elicitation/human_evaluation/config_leftovers.toml'


# python '/Users/micheleyin/Documents/Elicitation/human_evaluation/response_generation.py' -c '/Users/micheleyin/Documents/Elicitation/human_evaluation/config_leftovers.toml'


# copy batch 0 to user1
cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/0 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user3/3

# # copy batch 0 to user1
# cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/0 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user1/4
# # copy batch 1 to user2
# cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/1 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user2/4
# # copy batch 2 to user3
# cp -R /Users/micheleyin/Documents/Elicitation/human_evaluation/batches/2 /Users/micheleyin/Documents/Elicitation/human_evaluation/users/user3/4

# python '/Users/micheleyin/Documents/Elicitation/human_evaluation/black_list.py' '/Users/micheleyin/Documents/Elicitation/human_evaluation/users' '/Users/micheleyin/Documents/Elicitation/human_evaluation/blacklist.json'

# python '/Users/micheleyin/Documents/Elicitation/human_evaluation/add_user_batch.py' '/Users/micheleyin/Documents/Elicitation/human_evaluation/users'