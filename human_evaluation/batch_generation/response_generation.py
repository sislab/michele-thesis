import json
import os
import sys
import tomllib
import re
import pandas as pd
import evaluate
from argparse import ArgumentParser
import random
import shutil

def retrieve_data(input_files, with_prompt=False) -> dict:
    models_data = {}
    for file in input_files:
        with open(file,'rb') as f:
            data = json.load(f)
        model_name = data[0]["model_name"]
        models_data[model_name] = model_name
        narratives = data[0]["results"]
        inputs = []
        answers = []
        ids = []
        for narrative in narratives:
            try:
                input_text = " ".join(narrative["input"].split()) + " "
                text = " ".join(narrative["output"][0]["generated_text"].split()).strip()
                text = re.split(r'([?])', text)
                try:
                    text = text[0] + text[1]
                except IndexError:
                    text = text[0]
                text = text.replace("\\","") 
                text = text.strip()
                ids.append(narrative["id"])
            except TypeError:
                print(f"input text is not a string: {narrative}")
            if with_prompt:
                answer = text[text.find(input_text)+len(input_text)+len(' DOMANDA:'):].strip()
                # split by period, question mark, exclamation mark and keep the punctuation
                answer = re.split(r'([.!?])', answer)
                try:
                    answer = answer[0] + answer[1]
                except IndexError:
                    answer = answer[0]
            else:
                answer = text
            inputs.append(input_text)
            if len(answer) < 5:
                answer = "INVALID ANSWER"
            answers.append(answer)
        models_data[model_name] = {"inputs": inputs, "answers": answers,"ids": ids}
    return models_data
    
def retrieve_human_data(input_files) -> dict:
    models_data = {}
    for file in input_files:
        with open(file,'rb') as f:
            data = json.load(f)
        model_name = data[0]["model_name"]
        models_data[model_name] = model_name
        narratives = data[0]["results"]
        inputs = []
        answers = []
        ids = []
        for narrative in narratives:
            input_text = " ".join(narrative["input"].split()) + " "
            answer = [i["generated_text"] for i in narrative["output"]]
            inputs.append(input_text)
            answers.append(random.choice(answer))
            ids.append(narrative["id"])
        models_data[model_name] = {"inputs": inputs, "answers": answers, "ids": ids}
    return models_data

def response_retriever(input_files, human_path) -> list:
    """
    This function retrieves the responses from the specified models and saves them in a list in the Human Evaluation Protocol format
    """
    # retrieve model_responses
    model_data = retrieve_data(input_files, with_prompt=False)
    # retrieve human_responses
    human_data = retrieve_human_data(human_path)
    
    # zip everything toghever
    inputs = human_data["human"]["inputs"]
    human_answers = human_data["human"]["answers"]
    ids = human_data["human"]["ids"]
    model_responses = []
    model_ids = []
    model_names = []
    for model,values in model_data.items():
        model_responses.append(model_data[model]["answers"])
        model_ids.append(model_data[model]["ids"])
        model_names.append(model)

    list_of_possible_answers = []
    for a in zip(*model_responses,human_answers,ids, inputs):
        input_text = a[-1]
        id_narrative = a[-2]
        human_answer = a[-3]
        model_answer = a[:-3]
        set_of_answers = []
        for answer in model_answer: 
            part_id = f"model_name:[{str(model_names[model_answer.index(answer)])}]_narrative_id:[{str(id_narrative)}]"
            set_of_answers.append({"response_sample_id": part_id, "text": answer})
        set_of_answers.append({"response_sample_id": f"model_name:[human]_narrative_id:[{str(id_narrative)}]", "text": human_answer})
        list_of_possible_answers.append({"input": input_text, "answers": set_of_answers})
    return list_of_possible_answers
    # create a list of responses
    # {
    #     "response_sample_id": "71fa8eb9-43c5-4a6b-87f1-b1ba18df49bc",
    #     "text": "ho capito. forse potresti parlagli di come ti senti.",
    # }
    # save the list in the Human Evaluation Protocol format

def batch_splitter(responses, batch_size,n_overlap,shuffle=False) -> list[list]:
    # print(len(responses))
    # devo togliere n_overlap elementi dagli utimi batch sennò non ho abbastanza elementi
    shared = [responses.pop() for _ in range(n_overlap)]
    # num batches
    try: 
      n_batches = (len(responses) // (batch_size-n_overlap))
    except ZeroDivisionError:
      n_batches = len(responses)
    # print (n_batches)
    batches = [[] for _ in range(n_batches)]
    for batch in batches:
        for _ in range(batch_size-n_overlap):
            try:
                batch.append(responses.pop())
            except IndexError:
                print ("IndexError")
                break
    # add overlapping elements
    for share in shared:
        # take one element
        for batch in batches:
            batch.insert(0,share)
    if shuffle:
        for batch in batches:
            random.shuffle(batch)
    return batches
task_1 = {"id": "task_1",
      "question": "La continuazione proposta è grammaticalmente corretta?",
      "hint": "Identifichiamo se la continuazione contiene errori grammaticali o strutturali oppure se la continuazione è corretta.",
      "options": [
        {
          "value": "Corretta",
          "hint": "La continuazione non contiene nessuna tipologia di errore grammaticale o strutturale, nessuna ripetizione, errori di battitura o qualsiasi altro tipo di errore.",
          "explanations_options": False
        },
        {
          "value": "Non Corretta",
          "hint": "La continuazione contiene degli errori, per esempio ripetizioni, errori di ortografia, punteggiatura non esatta oppure qualsiasi altro tipo di errore.",
          "explanations_options": {
            "question": "Inserisci o seleziona una nota specifica relativa alla tua scelta (massimo 20 parole):",
            "options": [
              {
                "value": "errori_grammaticali",
                "text": "La continuazione contiene errori grammaticali."
              },
              {
                "value": "ripetizioni",
                "text": "Nella continuazione sono presenti una o più parti di frase ripetute."
              }
            ]
          }
        },
        {
          "value": "Non sono sicuro/a sia Corretta",
          "hint": "Alcuni elementi della continuazione sono corretti, tuttavia non riesco a decidere se è corretta oppure no.",
          "explanations_options": {
            "question": "Inserisci o seleziona una nota specifica relativa alla tua scelta (massimo 20 parole):",
            "options": []
          }
        }
      ]
    }
task_2 = {
      "id": "task_2",
      "question": "La continuazione proposta è appropriata?",
      "hint": "Identifichiamo quanto una continuazione è appropriata per un dato contesto oppure no, ossia se la continuazione è coerente e ha senso nel contesto corrente.",
      "options": [
        {
          "value": "Appropriata",
          "hint": "La continuazione ha senso e può essere la naturale continuazione del contesto del dialogo mostrato.",
          "explanations_options": {
            "question": "Inserisci o seleziona una nota specifica relativa alla tua scelta (massimo 20 parole):",
            "options": [
              {
                "value": "natural_continuation",
                "text": "La continuazione è la naturale continuazione del discorso perché c’è un chiaro filo logico."
              }
            ]
          }
        },
        {
          "value": "Non Appropriata",
          "hint": "La continuazione non ha senso tenendo conto del contesto del dialogo.",
          "explanations_options": {
            "question": "Inserisci o seleziona una nota specifica relativa alla tua scelta (massimo 20 parole):",
            "options": [
              {
                "value": "not_coherent",
                "text": "La continuazione non è coerente con quanto detto in precedenza nel contesto del dialogo."
              }
            ]
          }
        },
        {
          "value": "Non sono sicuro/a sia Appropriata",
          "hint": "La continuazione contiene alcuni elementi che hanno senso rispetto al contesto del dialogo, ma non riesco a decidere se è appropriata o no.",
          "explanations_options": {
            "question": "Inserisci o seleziona una nota specifica relativa alla tua scelta (massimo 20 parole):",
            "options": []
          }
        }
      ],
}
task_3 = {
      "id": "task_3",
      "question": "La continuazione contiene riferimenti al contesto del dialogo?",
      "hint": "Identifichiamo se la continuazione contiene riferimenti (impliciti o espliciti) al contesto del dialogo mostrato oppure no. ",
      "options": [
        {
          "value": "Contestualizzata",
          "hint": "La continuazione contiene riferimenti impliciti o espliciti al contesto del dialogo (eventi o persone o oggetti o emozioni).",
          "explanations_options": False
        },
        {
          "value": "Non Contestualizzata",
          "hint": "La continuazione non contiene riferimenti al contesto del dialogo in nessun modo.",
          "explanations_options": {
            "question": "Inserisci o seleziona una nota specifica relativa alla tua scelta (massimo 20 parole):",
            "options": [
              {
                "value": "no_references",
                "text": "Nonostante la continuazione sia corretta, non contiene nessun riferimento a quanto detto nel contesto o è generica. "
              },
              {
                "value": "wrong_references",
                "text": "La continuazione è in contrasto con le informazioni contenute nel contesto del dialogo."
              }
            ]
          }
        },
        {
          "value": "Non sono sicuro/a sia Contestualizzata",
          "hint": "La continuazione fa riferimento ad alcuni elementi, ma non riesco a decidere se può essere considerata contestualizzata oppure no.",
          "explanations_options": {
            "question": "Inserisci o seleziona una nota specifica relativa alla tua scelta (massimo 20 parole):",
            "options": []
          }
        }
      ]
    }
task_4 = {
      "id": "task_4",
      "question": "Quanto pensi che la persona A stia ascoltando la persona B?",
      "hint": "Identifichiamo quanto il partecipante A sta realmente ascoltando il partecipante B facendo attenzione al dialogo invece di rispondere in modo generico.",
      "options": [
        {
          "value": "Sta Ascoltando",
          "hint": "Il partecipante A ascolta con attenzione il partecipante B seguendo il dialogo, facendo riferimenti contestualizzati al contesto del dialogo e dimostrando di seguire il flusso del dialogo.",
          "explanations_options": False
        },
        {
          "value": "Non sta Ascoltando",
          "hint": "Il partecipante A sembra non prestare attenzione a ciò che il partecipante B sta dicendo, la continuazione è generica, non ci sono riferimenti al contesto del dialogo oppure riconoscimenti di quanto detto dal partecipante B.",
          "explanations_options": False
        },
        {
          "value": "Non sono sicuro/a stia Ascoltando",
          "hint": "Non è chiaro se il partecipante A sta ascoltando o meno il partecipante B, in alcuni elementi il partecipante A fa riferimenti a quanto detto da B ma successivamente propone elementi irrilevanti.",
          "explanations_options": False
        }
      ]}
def response_preparer(task:dict,sample_id:str, batch_id:str):
    data = {}
    data["history"] = [task["input"]]
    data["batch_id"] = batch_id
    data["sample_id"] = sample_id
    data["task"] = [task_1,task_2,task_3,task_4]
    for item in data["task"]:
        item["response_samples"] = task["answers"]
        if task["answers"] == None:
          print ("Error")
    return data
def response_filter(responses,blacklist):
    """
    This function filters the responses based on the blacklist
    """
    filtered_responses = []
    for response in responses:
        if response["answers"][0]["response_sample_id"].split(":")[-1][1:-1:] not in blacklist:
            filtered_responses.append(response)
    return filtered_responses
if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--config","-c", required=True, help="config", type = str)
    args = parser.parse_args()
    assert os.path.exists(args.config), "config file does not exist"
    assert os.path.isfile(args.config), "config file is not a file"
    assert args.config.endswith(".toml"), "config file is not a toml file"
    
    with open(args.config, "rb") as f:
        config = tomllib.load(f)
    model_files = config["file"]["input_model_files"]
    human_path = config["file"]["input_human_file"]
    output_path = config["file"]["output_path"]
    if os.path.exists(output_path):
      # delete the whole folder

      shutil.rmtree(output_path)
    black_list = config["file"]["blacklist_file"]
    batch_size = config["batches"]["batch_size"]
    shared = config["batches"]["shared_elements"]
    responses = response_retriever(model_files, [human_path])
    # remove elements from the blacklist
    with open(black_list,"rb") as f:
        blacklist = json.load(f)
    responses = response_filter(responses,blacklist)
    print (len(responses))   
    batches = batch_splitter(responses, batch_size, shared,shuffle=False)
    l = []
    for batch in batches:
        ids = [i["answers"][0]["response_sample_id"].split(":")[-1][1:-1:] for i in batch]
        print (ids)
        l.append(ids)
    # print (len(l))
    for idx,batch in enumerate(batches):
        path = f"{os.path.join(output_path,str(idx))}"
        # make dirs 
        os.makedirs(path,exist_ok=True)
        for i,item in enumerate(batch):
            ids = item["answers"][0]["response_sample_id"].split(":")[-1][1:-1:]
            data = response_preparer(item,ids,batch_id=f"batch{idx}")
            with open(os.path.join(path,f"{ids}.json"),"w") as f:
                json.dump(data,f, indent=4)
    print (len(responses))   