import json
import os
import sys
import tomllib
import re
import pandas as pd
import evaluate
from argparse import ArgumentParser
import random

if __name__ == "__main__":
    # list all files in sys.argv[1]
    ids = set()
    for path,folder,file in os.walk(sys.argv[1]):
        for f in file:
            ids.add(f.split('.')[0])
    with open(sys.argv[2],'w') as f:
        data = json.dump(list(ids),f,indent=4)