import json
import os
import sys
import tomllib
import re
import pandas as pd
import evaluate
from argparse import ArgumentParser
import random

if __name__ == "__main__":
    # list folders
    folders = [ f for f in os.listdir(sys.argv[1]) if os.path.isdir(os.path.join(sys.argv[1],f)) ]
    # print (folders)
    for folder in folders:
        path = os.path.join(sys.argv[1],folder)
        batches = [ f for f in os.listdir(path) if os.path.isdir(os.path.join(path,f)) ]
        batches.sort(key = lambda x: int(x))
        # print (batches)
        for batch in batches:
            batch_path = os.path.join(path,batch)
            items = [ f for f in os.listdir(batch_path) if os.path.isfile(os.path.join(batch_path,f)) ]
            for item in items:
                item_path = os.path.join(batch_path,item)
                print (item_path)
                with open(item_path,'r') as f:
                    data = json.load(f)
                for task in data["task"]:
                    task["id"] = f"{task['id']}-{data['batch_id']}-{data['sample_id']}"
                    # task["id"] = "-".join(task["id"].split("-")[:-2]) + f"-sample{data['sample_id']}"
                with open(item_path,'w') as f:
                    json.dump(data,f,indent=4)