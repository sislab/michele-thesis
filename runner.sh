conda activate finetuning

python finetuning/narrativeDatasetTrainCausalLMOptuna.py --config '/home/michele/LLMs-runner/finetuning/configCausalLM.toml'

python finetuning/narrativeDatasetTrainSeq2SeqLMOptuna.py --config '/home/michele/LLMs-runner/finetuning/configSeq2SeqLM.toml'